/**
 * 掌上山科 - 入口
 * https://www.ifeipeng.com
 * @飞蓬
 */

import { AppRegistry } from 'react-native';
import App from './src/index';
// import App from './App'

/**
 * @Author   Menger
 * @DateTime 2017-11-24
 * 正式发布版本屏蔽控制台打印
 */
if ( !__DEV__ ) {
	global.console = {
		log: () => {},
		info: () => {},
		warn: () => {},
		error: () => {},
		gruop: () => {},
		gruopEnd: () => {},
	};
};

console.ignoredYellowBox = ['Remote debugger is in'];

AppRegistry.registerComponent('Sdust', () => App);
