/**
 * 速芽物流 - NAVIGATOR
 * https://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
	View,
} from 'react-native'
import { StackNavigator, TabNavigator, TabBarBottom } from 'react-navigation'
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator'
import GlobalStyles from '../constants/GlobalStyles'

import stroage from '../store'
import '../store/Global'
import { consoleLog } from '../utils/utilsToast'

import TabBarItem from '../components/common/TabBarItem'
import ShareUtils from '../components/common/shareUtils'
import WebViewPage from '../pages/common/webview'

import Login from '../pages/login'
import Register from '../pages/login/register'
import Repassword from '../pages/login/repassword'

import Home from '../pages/home'
import Detail from '../pages/home/detail'
import NewsWebDetail from '../pages/home/newsWebDetail'
import MyNewsWebDetail from '../pages/home/myNewsWebDetail'
import VideoWebDetail from '../pages/home/videoWebDetail'
import PicWebDetail from '../pages/home/picWebDetail'

import Student from '../pages/student'
import Teacher from '../pages/teacher'
import Gongju from '../pages/gongju'
import Mall from '../pages/mall'
import Chat from '../pages/chat'
import ChatPage1 from '../pages/chat/chatPage1'
import ChatPage2 from '../pages/chat/chatPage2'
import ChatPage3 from '../pages/chat/chatPage3'
import ChatPage4 from '../pages/chat/chatPage4'
import ChatPage5 from '../pages/chat/chatPage5'
import Fatie from '../pages/chat/fatie'

import User from '../pages/user'
import Yonghuxieyi from '../pages/user/yonghuxieyi'
import Shoucang from '../pages/user/shoucang'
import About from '../pages/user/about'
import Ziliao from '../pages/user/ziliao'
import Shezhi from '../pages/user/shezhi'
import SetMobile from '../pages/user/setMobile'
import SetPassword from '../pages/user/setPassword'

const TabNavScreen = TabNavigator(
	{
		Home: {
			screen: Home,
			navigationOptions: ({ navigation }) => ({
				header: null,
				tabBarLabel: '首页',
				tabBarIcon: ({focused, tintColor}) => (
					<TabBarItem
						subScript = {false}
						tintColor = {tintColor}
						focused = {focused}
						normalImage = {require('../assets/images/tabbar/icon_tabbar_home.png')}
						selectedImage = {require('../assets/images/tabbar/icon_tabbar_home_cur.png')}
					/>
				),
			}),
		},
		Mall: {
			screen: Mall,
			navigationOptions: ({ navigation }) => ({
				header: null,
				tabBarLabel: '商城',
				tabBarIcon: ({focused, tintColor}) => (
					<TabBarItem
						subScript = {false}
						tintColor = {tintColor}
						focused = {focused}
						normalImage = {require('../assets/images/tabbar/icon_tabbar_mall.png')}
						selectedImage = {require('../assets/images/tabbar/icon_tabbar_mall_cur.png')}
					/>
				),
			}),
		},
		User: {
			screen: User,
			navigationOptions: ({ navigation }) => ({
				header: null,
				tabBarLabel: '我的',
				tabBarIcon: ({focused, tintColor}) => (
					<TabBarItem
						subScript = {false}
						tintColor = {tintColor}
						focused = {focused}
						normalImage = {require('../assets/images/tabbar/icon_tabbar_user.png')}
						selectedImage = {require('../assets/images/tabbar/icon_tabbar_user_cur.png')}
					/>
				),
			}),
		},
	},
	{
		initialRouteName: 'Home',
		tabBarComponent: TabBarBottom,
		tabBarPosition: 'bottom',
		swipeEnabled: true,
		tabBarOptions: {
			activeTintColor: '#0068B7',
			inactiveTintColor: '#a4aab3',
			style: { backgroundColor: '#ffffff' },
			labelStyle: { fontSize: 12, marginBottom: 4,}
		},
	}

);

const App = StackNavigator(
	{
		TabNavScreen: {
			screen: TabNavScreen
		},
		WebViewPage: {
			screen: WebViewPage,
			navigationOptions: ({navigation}) => ({
				header: null,
			}),
		},
		Login: {
			screen: Login,
			navigationOptions: ({navigation}) => ({
				header: null,
				title: '账号登录',
			}),
		},
		Register: {
			screen: Register,
			navigationOptions: ({navigation}) => ({
				title: '账号注册',
			}),
		},
		Repassword: {
			screen: Repassword,
			navigationOptions: ({navigation}) => ({
				title: '忘记密码',
			}),
		},
		Student: {
			screen: Student,
			navigationOptions: ({navigation}) => ({
				header: null,
				title: '学生入口',
			}),
		},
		Teacher: {
			screen: Teacher,
			navigationOptions: ({navigation}) => ({
				title: '教师入口',
			}),
		},
		User: {
			screen: User,
			navigationOptions: ({navigation}) => ({
				title: '会员中心',
			}),
		},
		Home: {
			screen: Home,
			navigationOptions: ({navigation}) => ({
				title: '会员中心',
			}),
		},
		Chat: {
			screen: Chat,
			navigationOptions: ({navigation}) => ({
				title: '会员中心',
			}),
		},
		ChatPage1: {
			screen: ChatPage1,
			navigationOptions: ({navigation}) => ({
				title: '会员中心',
			}),
		},
		ChatPage2: {
			screen: ChatPage2,
			navigationOptions: ({navigation}) => ({
				title: '会员中心',
			}),
		},
		ChatPage3: {
			screen: ChatPage3,
			navigationOptions: ({navigation}) => ({
				title: '会员中心',
			}),
		},
		ChatPage4: {
			screen: ChatPage4,
			navigationOptions: ({navigation}) => ({
				title: '会员中心',
			}),
		},
		ChatPage5: {
			screen: ChatPage5,
			navigationOptions: ({navigation}) => ({
				title: '会员中心',
			}),
		},
		Fatie: {
			screen: Fatie,
			navigationOptions: ({navigation}) => ({
				title: '会员中心',
			}),
		},
		Yonghuxieyi: {
			screen: Yonghuxieyi,
			navigationOptions: ({navigation}) => ({
				title: '会员中心',
			}),
		},
		Shoucang: {
			screen: Shoucang,
			navigationOptions: ({navigation}) => ({
				title: '我的收藏',
			}),
		},
		About: {
			screen: About,
			navigationOptions: ({navigation}) => ({
				title: '关于我们',
			}),
		},
		Ziliao: {
			screen: Ziliao,
			navigationOptions: ({navigation}) => ({
				title: '关于我们',
			}),
		},
		Shezhi: {
			screen: Shezhi,
			navigationOptions: ({navigation}) => ({
				title: '关于我们',
			}),
		},
		SetMobile: {
			screen: SetMobile,
			navigationOptions: ({navigation}) => ({
				title: '关于我们',
			}),
		},
		SetPassword: {
			screen: SetPassword,
			navigationOptions: ({navigation}) => ({
				title: '关于我们',
			}),
		},
		Detail: {
			screen: Detail,
			navigationOptions: ({navigation}) => ({
				title: '关于我们',
			}),
		},
		NewsWebDetail: {
			screen: NewsWebDetail,
			navigationOptions: ({navigation}) => ({
				title: '关于我们',
			}),
		},
		MyNewsWebDetail: {
			screen: MyNewsWebDetail,
			navigationOptions: ({navigation}) => ({
				title: '关于我们',
			}),
		},
		VideoWebDetail: {
			screen: VideoWebDetail,
			navigationOptions: ({navigation}) => ({
				title: '关于我们',
			}),
		},
		PicWebDetail: {
			screen: PicWebDetail,
			navigationOptions: ({navigation}) => ({
				title: '关于我们',
			}),
		},
		ShareUtils: {
			screen: ShareUtils,
			navigationOptions: ({navigation}) => ({
				title: '关于我们',
			}),
		},
	},
	{
		mode: 'card',
		headerMode: 'screen',
		initialRouteName: 'Login',
		navigationOptions: {
			header: null,
			headerBackTitle: null,
			headerTintColor: GlobalStyles.themeColor,
			headerStyle: {
				backgroundColor: '#fff',
			},
			showIcon: true,
			headerTitleStyle: {
				alignSelf: 'center',
			},
			headerRight: (
				<View />
			),
		},
		transitionConfig:()=>({
			screenInterpolator: CardStackStyleInterpolator.forHorizontal,
		}),
	}
);
const defaultGetStateForAction = App.router.getStateForAction;

App.router.getStateForAction = (action, state) => {

	if (action.routeName === 'User' && !global.user.loginState) {
		this.routes = [
			...state.routes,
			{
				key: 'id-' + Date.now(),
				routeName: 'Login',
				params: {
					name: 'name1'
				}
			},
		];
		return {
			...state,
			routes,
			index: this.routes.length - 1,
		};
	}
	return defaultGetStateForAction(action, state);
};

export default App;