import Toast from 'react-native-root-toast';

let toast;
const loggerTrueColor = 'color: #1ba01b';
const loggerFalseColor = 'color: #f00';

export const toastShort = (content) => {
	if (toast !== undefined) {
		Toast.hide(toast);
	}
	toast = Toast.show(content.toString(), {
		duration: Toast.durations.SHORT,
		position: Toast.positions.CENTER,
		shadow: false,
		animation: true,
		hideOnPress: true,
		delay: 0
	});
};

export const toastLong = (content) => {
	if (toast !== undefined) {
		Toast.hide(toast);
	}
	toast = Toast.show(content.toString(), {
		duration: Toast.durations.LONG,
		position: Toast.positions.CENTER,
		shadow: false,
		animation: true,
		hideOnPress: true,
		delay: 0
	});
};

export const consoleLog = (tips, content) => {
	tips = tips ? tips : '模块打印';
	try {
		console.group('%c'+ tips, loggerTrueColor);
		console.log( '%c返回结果——>>', loggerTrueColor, content);
		console.groupEnd();
	} catch (e) {
		// 非调试模式，无法使用group
	}
};