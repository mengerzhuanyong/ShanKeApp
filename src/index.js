/**
 * 速芽物流 - App主页面
 * https://menger.me
 * @大梦
 */

import React, { Component } from 'react'
import {
   Platform
} from 'react-native'
import NetRequest from './utils/utilsRequest'
import { NetApi } from './constants/GlobalApi'
import { Constants } from './constants/Constants'
import AppNavigation from './routers'
import AppNavigations from './routers/indexs'
import SplashScreen from 'react-native-splash-screen'
import JShareModule from 'jshare-react-native';
import *as wechat from 'react-native-wechat'

export default class Index extends Component {
	constructor(props) {
		super(props);
		this.state =  {
			is_online: '',
            version: ''
		}
		this.netRequest = new NetRequest();
	}

    async componentWillMount(){
        try {
            await this.getOnlineStatus();
        } catch (error) {
            // // console.log(error);
        }
    }

	componentDidMount (){
        this.timer = setTimeout(() => {
            SplashScreen.hide();
        }, 1500);
        // wechat.registerApp('wx2f01e40bf0d7e836');
        if (Platform.OS === 'ios') {
            // 启动极光分享 ios需要
            JShareModule.setup()
        }
        
	}

    componentWillUnmount() {
        this.timer && clearTimeout(this.timer);
    }

    getOnlineStatus = () => {
        let url = NetApi.is_online;
        this.netRequest.fetchGet(url, true)
            .then( result => {
                console.log(result);
                this.setState({
                 is_online: result.data,
                 version: result.version
                })
            })
            .catch( error => {
                consoleLog('链接服务器出错，请稍后重试', error);
            })
    }
    
    // 苹果 0 加s  1 无s
    // 安卓 都改成不加s的
	render() {
        if (this.state.is_online == 0 && this.state.version == Constants.VERSION_NAME) {
            return <AppNavigation />
        }else{
            return <AppNavigation />
        }     
	}

};