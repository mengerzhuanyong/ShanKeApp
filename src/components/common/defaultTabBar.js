import React, { PureComponent } from 'react';
import GlobalStyles from '../../constants/GlobalStyles'
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  View,
  Animated,
  TouchableOpacity,
  TouchableWithoutFeedback
} from 'react-native';
// const Button = require('./Button');
const marginLeft = 0
const tabBackWidth = GlobalStyles.width - marginLeft * 2;
class DefaultTabBar extends PureComponent {
  static propTypes = {
    goToPage: PropTypes.func,
    activeTab: PropTypes.number,
    tabs: PropTypes.array,
    backgroundColor: PropTypes.string,
    activeTextColor: PropTypes.string,
    inactiveTextColor: PropTypes.string,
    textStyle: Text.propTypes.style,
    tabStyle: View.propTypes.style,
    renderTab: PropTypes.func,
    underlineStyle: View.propTypes.style,
  }
  static defaultProps = {
    activeTextColor: '#fff',
    inactiveTextColor: '#80C5A1',
    backgroundColor: 'transparent',
  }

  renderTab = (name, page, isTabActive, onPressHandler) => {
    console.log(this.props)
    const { activeTextColor, inactiveTextColor, textStyle, } = this.props;
    const textColor = isTabActive ? activeTextColor : inactiveTextColor;
    const fontWeight = isTabActive ? 'bold' : 'normal';
    return (
      <TouchableWithoutFeedback
        key={name}
        onPress={() => onPressHandler(page)}
      >
        <View
          style={[styles.tab, this.props.tabStyle,]}
        >
          <Text style={[{ color: textColor, fontWeight, }, textStyle,]}>
            {name}
          </Text>
        </View>
      </TouchableWithoutFeedback>)
  }

  render() {
    const containerWidth = this.props.containerWidth;
    const numberOfTabs = this.props.tabs.length;
    const tabUnderlineStyle = {
      position: 'absolute',
      width: (tabBackWidth - 80) / numberOfTabs,
      height: 2,
      backgroundColor: '#fff',
      bottom: 0,
      left: 0 - 3 - ((tabBackWidth - 80) / numberOfTabs - 32) / 2 
      // left: GlobalStyles.width / numberOfTabs / 2,
      // marginLeft: 0 - ((tabBackWidth - 0) / numberOfTabs / 2)
    };

    const translateX = this.props.scrollValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0 + 20, tabBackWidth / numberOfTabs + 20],
    });
    return (
      <View style={[styles.tabs, { backgroundColor: this.props.backgroundColor, }, this.props.style,]}>
        <View style={[styles.tabBack,]}>
          {this.props.tabs.map((name, page) => {
            const isTabActive = this.props.activeTab === page;
            const renderTab = this.props.renderTab || this.renderTab;
            return renderTab(name, page, isTabActive, this.props.goToPage);
          })}
        </View>
        <Animated.View
          style={[
            tabUnderlineStyle,
            {
              transform: [
                { translateX },
              ]
            },
            this.props.underlineStyle,
          ]}
        />
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 10,
    // backgroundColor: 'blue',
  },
  tabs: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderBottomColor: '#f2f2f2',
    borderBottomWidth: 1,
    // backgroundColor: 'blue',
  },
  tabBack: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: tabBackWidth,
    marginTop: 10,
    // backgroundColor: 'red',
  }
});

//make this component available to the app
export default DefaultTabBar;
