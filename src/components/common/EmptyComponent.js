/**
 * 速芽物流 - BANNER
 * https://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
	Text,
	View,
	Image,
	TextInput,
	StyleSheet,
	TouchableOpacity
} from 'react-native'

export default class Banner extends Component {

	render(){
		return (
			<View style={styles.container}>
				<Text style={styles.title}>暂未找到相关信息！</Text>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		height: 100,
		alignItems: 'center',
		justifyContent: 'center',
	},
	title: {
		fontSize: 16,
		color: '#888',
	}
});