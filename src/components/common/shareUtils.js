/**
 * 鼎亚汽车 - 我的
 * https://menger.me
 * @大梦
 */

import React, { Component } from 'react';
import {
	Text,
	View,
	Alert,
	Modal,
	Image,
	StyleSheet,
	TouchableOpacity,
	Dimensions,
} from 'react-native';

import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'
import *as wechat from 'react-native-wechat'
var {width,height} = Dimensions.get('window');
import JShareModule from 'jshare-react-native';


export default class ShareUtils extends Component {

	constructor(props) {
		super(props);
		this.state = {
			isVisible: this.props.show,
			id: this.props.id,
			title: this.props.title,
			description: this.props.description,
			thumbImage: this.props.thumbImage,
			webpageUrl: this.props.webpageUrl,
			webview: this.props.webview,
			hasRefresh: this.props.hasRefresh,
			hasCollect: this.props.hasCollect,
			isCollected: this.props.isCollected,
		};
		this.netRequest = new NetRequest();
	}

	static defaultProps = {
	  collectNews: () => {}
	}

	shouldcomponentUpdate(nextProps, nextState) {
		if (this.state.isCollected != nextState.isCollected) {
			return true;
		}
		return false;
	}

	componentWillReceiveProps(nextProps) {
		consoleLog('poup', nextProps);
		this.setState({
			isVisible: nextProps.show,
			isCollected: nextProps.isCollected,
		});
	}

	closeModal() {
		this.setState({
			isVisible: false
		});
		this.props.closeModal(false);
	}

	// _shareToTimeline = () => {
	// 	wechat.isWXAppInstalled()
	// 		.then((isInstalled) => {

	// 			if(isInstalled) {

	// 				wechat.shareToTimeline({
	// 					type: 'news',
	// 					title: this.state.title,
	// 					description: this.state.description,
	// 					thumbImage: this.state.thumbImage,
	// 					webpageUrl: this.state.webpageUrl,
	// 				});
	// 				this.closeModal();

	// 			}else{
	// 				toastShort('没有安装微信软件，请您安装微信之后再试');
	// 				this.closeModal();
	// 			}
	// 		})
	// 		.catch((error) => {

	// 			toastShort(error.message);
	// 			this.closeModal();

	// 		});
	// }


	// _shareToSession = () => {
	// 	wechat.isWXAppInstalled()
	// 		.then((isInstalled) => {

	// 			if(isInstalled) {

	// 				wechat.shareToSession({
	// 					type: 'news',
	// 					title: this.state.title,
	// 					description: this.state.description,
	// 					thumbImage: this.state.thumbImage,
	// 					webpageUrl: this.state.webpageUrl,
	// 				});
	// 				this.setState({
	// 					isVisible: false
	// 				});

	// 			}else{
	// 				toastShort('没有安装微信软件，请您安装微信之后再试');
	// 				this.closeModal();
	// 			}
	// 		})
	// 		.catch((error) => {

	// 			toastShort(error.message);
	// 			this.closeModal();

	// 		});
	// }

	// _shareToQQ = () => {
	// 	toastShort('请您安装最新版本QQ之后再试');
	// 	this.closeModal();
	// }


	// _shareToQQZone = () => {
	// 	toastShort('请您安装最新版本QQ之后再试');
	// 	this.closeModal();
	// }


	// _shareToWeibo = () => {
	// 	toastShort('请您安装最新版本微博之后再试');
	// 	this.closeModal();
	// }


	toShare = (type) => {
        const params = {
            type: 'link',
            platform: type,  // 分享到指定平台
            url: this.state.webpageUrl,
            title: this.state.title,
            text: this.state.description,
            imageUrl:this.state.thumbImage
        }
        this.closeModal();
        JShareModule.share(params, (success) => {
            console.log(success)
        }, (error) => {
            console.log(error)
        })
	}




	toLogin = () => {
		const { navigate } = this.props.navigation;
		navigate('Login', {
			
		})
	}

	refresh = () => {
		this.closeModal();
		let message = 'refresh'
		this.state.webview.postMessage(message);
    	// toastShort('已为您重新加载数据~');
	}



	collectNews = (aid) => {
		if (!global.user.loginState) {
			this.toLogin();
			this.closeModal();
		}else {
			let uid = global.user.userData.id;
	        let url = NetApi.collect + '/uid/' + uid + '/aid/' + aid;
	        
	        this.netRequest.fetchGet(url, 'true')
	            .then( result => {
	                console.log(result);
	                if(result.code == 1){
	                	toastShort('收藏成功，可到个人中心查看');
	                	this.props.reloadNetData();
	                	this.setState({
	                		isCollected: 1,
	                	});
	                	this.closeModal();
	                }
	                if(result.code == 0){
	                	toastShort('您已取消收藏');
	                	this.props.reloadNetData();
	                	this.setState({
	                		isCollected: 0,
	                	});
	                	this.closeModal();
	                }
	            })
	            .catch( error => {
	                consoleLog('链接服务器出错，请稍后重试', error);
	            })
		}

			
	}

	renderDialog() {
		return (
			<View style={styles.shareView}>
				<View style={styles.topView}>
					<View style={styles.shareViewContent}>
						<TouchableOpacity style={styles.shareItem} onPress={() => {this.toShare('wechat_session')}}>
							<View style={styles.imgWrap}><Image source={require('../../assets/images/icons/icon_share_haoyou.png')} style={styles.shareItemIcon} /></View>
							<Text style={styles.shareItemTitle}>微信</Text>
						</TouchableOpacity>
						<TouchableOpacity style={styles.shareItem} onPress={() => {this.toShare('wechat_timeLine')}}>
							<View style={styles.imgWrap}><Image source={require('../../assets/images/icons/icon_share_pengyouquan.png')} style={styles.shareItemIcon} /></View>
							<Text style={styles.shareItemTitle}>朋友圈</Text>
						</TouchableOpacity>
						<TouchableOpacity style={styles.shareItem} onPress={() => {this.toShare('qq')}}>
							<View style={styles.imgWrap}><Image source={require('../../assets/images/icons/icon_share_qq.png')} style={styles.shareItemIcon} /></View>
							<Text style={styles.shareItemTitle}>QQ</Text>
						</TouchableOpacity>
						<TouchableOpacity style={styles.shareItem} onPress={() => {this.toShare('qzone')}}>
							<View style={styles.imgWrap}><Image source={require('../../assets/images/icons/icon_share_kongjian.png')} style={styles.shareItemIcon} /></View>
							<Text style={styles.shareItemTitle}>QQ空间</Text>
						</TouchableOpacity>
						<TouchableOpacity style={styles.shareItem} onPress={() => {this.toShare('sina_weibo')}}>
							<View style={styles.imgWrap}><Image source={require('../../assets/images/icons/icon_share_weibo.png')} style={styles.shareItemIcon} /></View>
							<Text style={styles.shareItemTitle}>微博</Text>
						</TouchableOpacity>
					</View>
					<View style={styles.collectContent}>
						{this.state.hasCollect ?
							<TouchableOpacity style={styles.collectItem} onPress={() => this.collectNews(this.state.id)}>
								{this.state.isCollected == 1 ?
									<View style={styles.imgWrap}><Image source={require('../../assets/images/icons/icon_collect_cur.png')} style={styles.collectItemIcon} /></View>
								:
									<View style={styles.imgWrap}><Image source={require('../../assets/images/icons/icon_collect.png')} style={styles.collectItemIcon} /></View>
								}
								<Text style={styles.collectItemTitle}>收藏</Text>
							</TouchableOpacity>
						:
							null
						}
						{this.state.hasRefresh ?
							<TouchableOpacity style={styles.collectItem} onPress={() => {this.refresh()}}>
								<View style={styles.imgWrap}><Image source={require('../../assets/images/icons/icon_shuaxin2.png')} style={styles.collectItemIcon} /></View>
								<Text style={styles.collectItemTitle}>刷新</Text>
							</TouchableOpacity>
						:
							null
						}
					</View>
				</View>
				<View style={styles.bottomView}>
					<Text style={styles.btnItem}>取消</Text>
				</View>
			</View>
		)
	}

	render() {

		return (
			<View style={styles.container}>
				<Modal
					transparent={true}
					visible={this.state.isVisible}
					animationType={'fade'}
					onRequestClose={() => this.closeModal()}
				>
					<TouchableOpacity
						style={styles.shareContainer}
						activeOpacity={1}
						onPress={() => this.closeModal()}
					>
						{this.renderDialog()}
					</TouchableOpacity>
				</Modal>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	shareContainer: {
		flex: 1,
		backgroundColor: 'rgba(0,0,0,0.5)',
	},
	shareView: {
		bottom: 0,
		position: "absolute",
		width:width,
	},
	topView: {
		backgroundColor: '#f8f8f8',
		paddingBottom: 10,
		paddingTop: 10
	},
	titleView: {
		height: 60,
		alignItems: 'center',
		justifyContent: 'center',
	},
	shareViewTitle: {
		fontSize: 16,
		color: '#888',
		borderColor: '#aaa',
		borderBottomWidth: 1,
	},
	shareViewContent: {
		marginTop: 10,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent:'center',
		borderBottomWidth: 1,
		borderBottomColor: '#ececec',
		paddingBottom: 15
	},
	shareItem: {
		backgroundColor: '#f8f8f8',
		flex: 1,
		alignItems:'center',
	},
	shareItemIcon: {
		width: 32,
		height: 32,
		resizeMode: 'contain',
		position: 'absolute',
		top: 14,
		left: 14,

	},
	shareItemTitle: {
		fontSize: 14,
		color: '#666',
		height: 30,
		lineHeight: 30
	},
	bottomView: {
		height: 60,
		alignItems: 'center',
		backgroundColor: '#fff',
		justifyContent: 'center',
	},
	btnItem: {
		fontSize: 18,
		color: '#333',
	},
	collectContent: {
		marginTop: 15,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent:'flex-start',
		
	},
	collectItem: {
		width: width*0.2,
		alignItems:'center',
	},
	collectItemIcon: {
		width: 30,
		height: 30,
		resizeMode: 'contain',
		position: 'absolute',
		top: 15,
		left: 15,

	},
	collectItemTitle: {
		fontSize: 14,
		color: '#666',
		height: 30,
		lineHeight: 30

	},
	imgWrap: {
		backgroundColor: '#fff',
		width: 60,
		height: 60,
		borderRadius: 30,
		position: 'relative',

	}
});