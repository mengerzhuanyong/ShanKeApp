/**
 * 速芽物流 - BANNER
 * https://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
	Text,
	View,
	Image,
	TextInput,
	StyleSheet,
	TouchableOpacity
} from 'react-native'

import Swiper from 'react-native-swiper'
import { toastShort, consoleLog } from '../../utils/utilsToast'
import { NetApi } from '../../constants/GlobalApi'
import NetRequest from '../../utils/utilsRequest'
import GlobalStyles from '../../constants/GlobalStyles'


export default class Banner extends Component {

	constructor(props){
		super(props);
		this.state = {
			swiperShow: false,
			banners: [],
		};
		this.netRequest = new NetRequest();
	}

	componentDidMount() {
        this.loadNetData();
		setTimeout(() => {
			this.setState({
				swiperShow: true
			});
		}, 0)
	}

	componentWillReceiveProps(nextProps){
		consoleLog('首页轮播', nextProps);
		if (nextProps.bannerData) {
			this.updateState({
				banners: nextProps.bannerData
			})
		}
	}

	updateState = (state) => {
		if (!this) { return };
		this.setState(state);
	}

	loadNetData = () => {
		let url = NetApi.newslide;
        this.netRequest.fetchGet(url)
            .then( result => {
                console.log(result.data);
                this.setState({
                	banners: result.data
                });
            })
            .catch( error => {
                consoleLog('链接服务器出错，请稍后重试', error);
            })
	}

	toWebview = (title, id, link, compent) => {
		const { navigate } = this.props.navigation;
		navigate(compent, {
			title:title,
			id: id,
			link:link,
		})
	}

	renderBanner = (row) => {
		if(this.state.swiperShow) {
			console.log(row);
			console.log(row.length);
			if(row.length <= 0) {
				return null;
			}
			let banners = row.map((obj,index)=>{
				let url = NetApi.base+NetApi.newsdetail+'/id/'+this.state.banners[index].id+'/isapp/2';
				return (
					<TouchableOpacity
						style={GlobalStyles.bannerViewWrap}
						key={"bubble_"+index}
						activeOpacity = {1}
						onPress={()=>{this.toWebview(this.state.banners[index].title, this.state.banners[index].id, url, 'NewsWebDetail')}}
					>
						<View style={GlobalStyles.bannerViewWrap}>
							<Image source={{uri:this.state.banners[index].logo}} style={GlobalStyles.bannerImg} />
							<View style={GlobalStyles.bannerShadow}></View>
							<Text style={GlobalStyles.bannerTxt}>{this.state.banners[index].title}</Text>
						</View>
					</TouchableOpacity>
				)
			});
			return (
				<Swiper
					index={0}
					loop={true}
					autoplay={true}
					horizontal={true}
					removeClippedSubviews={false}
					showsPagination={false}
					autoplayTimeout={5}
					height={210}
					width={GlobalStyles.width}
					style={{paddingTop:0,marginTop:0}}
					lazy={true}
					dot = {<View style={GlobalStyles.bannerDot} />}
					activeDot = {<View style={GlobalStyles.bannerActiveDot} />}
				>
					{banners}
				</Swiper>
			)
		}
	}

	render(){
		const { banners } = this.state;
		console.log(banners);
		return (
			<View style={GlobalStyles.bannerContainer}>
				{this.renderBanner(banners)}
			</View>
		);
	}
}

const styles = StyleSheet.create({

});