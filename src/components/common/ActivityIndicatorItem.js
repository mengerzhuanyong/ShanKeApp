/**
 * 速芽物流 - ActivityIndicatorItem
 * https://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
	StyleSheet,
	ActivityIndicator,
	View,
	Image
} from 'react-native'
import GlobalStyles from '../../constants/GlobalStyles'

export default class ActivityIndicatorItem extends Component {

	render(){
		const { color } = this.props;
		return (
			<View style={GlobalStyles.loadingWrap}><Image source={require('../../assets/images/icons/loading.gif')} style={GlobalStyles.loadingIco} /></View>
		);
	}
}

const styles = StyleSheet.create({	
	activityIndicator: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
});