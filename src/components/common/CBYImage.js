/**
 * Created by eric on 2018/2/1.
 */

import React from "react";
import {Image, Platform} from 'react-native';
import PropTypes from "prop-types"
var resolveAssetSource = require('resolveAssetSource');
export default class CBYImage extends Image {
    static propTypes = {
        ...Image.propTypes,
        selected:PropTypes.bool
    }
    viewConfig = Object.assign({} , this.viewConfig, {
        validAttributes: Object.assign(
            {},
            this.viewConfig.validAttributes,
            {[Platform.OS === 'ios' ? 'source' : 'src']: true})
    });

    constructor(props) {
        super(props);
        this.setNativeProps = (props = {}) => {

            if (props.source) {
                const source = resolveAssetSource(props.source);
                let sourceAttr = Platform.OS === 'ios' ? 'source' : 'src';
                let sources;
                if (Array.isArray(source)) {
                    sources = source;
                } else {
                    sources = [source];
                }
                Object.assign(props, {[sourceAttr]: sources});
            }

            return super.setNativeProps(props);
        }
    }
}
