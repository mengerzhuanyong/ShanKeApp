'use strict'

import React, {Component} from 'react'
import {
    StyleSheet,
    ActivityIndicator
} from 'react-native'
import GlobalStyles from '../../constants/GlobalStyles'

import AlertContent from './AlertContent';
import { bouncer } from '../../utils/Tool';
import { Overlay } from 'teaset';

class AlertManager {

    static popViewRefs = []

    // // params = {
    //     title: '温馨提示asdasd',
    //     titleStyle: {},
    //     detail: '详细描述',
    //     detailStyle: {},
    //     actions: [
    //         { title: '取消', titleStyle: {}, onPress: () => alert('取消') },
    //         { title: '确定', titleStyle: {}, onPress: () => alert('取消') },
    //     ]
    // }
    static show(params,option={}) {
        this.showPopView(<AlertContent {...params} />, option)
    }

    static showPopView(component, option = {}) {
        this.popViewRefs = bouncer(this.popViewRefs.slice()) // 过滤
        if (this.popViewRefs.length === 0) {
            Overlay.show(
                <Overlay.PopView
                    ref={v => this.popViewRefs.push(v)}
                    style={{ justifyContent: 'center', alignItems: 'center', }}
                    type={'zoomOut'}
                    modal={false}
                    onCloseRequest={() => this.hide()}
                    {...option}
                >
                    {component}
                </Overlay.PopView>
            )
        }
    }

    static hide() {
        this.popViewRefs = bouncer(this.popViewRefs.slice()) // 过滤
        if (this.popViewRefs.length > 0) {
            const lastRef = this.popViewRefs.pop()
            lastRef.close()
        }
    }

}

//make this component available to the app
export default AlertManager;