/**
 * 掌上山科APP
 * http://www.sdust.edu.cn/
 * 山科技术团队倾情打造
 */

import React, {Component} from 'react'
import {
	Text,
	View,
	Image,
	FlatList,
	TextInput,
	ScrollView,
	StyleSheet,
	TouchableOpacity,
	Alert,
	ActivityIndicator,
	InteractionManager
} from 'react-native'

import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import { toastShort, consoleLog } from '../../utils/utilsToast'
import ActivityIndicatorItem from '../../components/common/ActivityIndicatorItem'
import *as wechat from 'react-native-wechat'
import ShareUtils from '../../components/common/shareUtils'

import FooterComponent from '../../components/common/footerComponent'

export default class PicTabItemView extends Component {
	constructor(props) {
		super(props);
		this.state =  {
			ready: false,
            showFoot: 0,
            error: false,
            errorInfo: "",
			loadMore: false,
			refreshing: false,
			companyListData: [],
			title: '',
			id: 0,
            showSharePop: false,
            description: '',
            webpageUrl: '',
            logo: '',
            hasRefresh: false,
            hasCollect: true,
            isCollected: 0,
		}
		this.netRequest = new NetRequest();
	}

    /**
     * 初始化状态
     * @type {Boolean}
     */
    page = -1;
    totalPage = 0;
    loadMore = false;
    refreshing = false;

	async componentDidMount(){
		await this.dropLoadMore();
		setTimeout(() => {
			this.updateState({
				ready: true,
            	showFoot: 0 // 控制foot， 0：隐藏footer  1：已加载完成,没有更多数据   2 ：显示加载中
			})
		},0)
	}

	onSharePress = (data, id, isCollected) => {
		this.setState({
            showSharePop: !this.state.showSharePop,
            id: id,
            description: data.description,
    		webpageUrl: data.webpageUrl,
    		logo: NetApi.root + 'logo120.png',
    		title: data.title,
    		isCollected: isCollected,
        });
	}

	updateState = (state) => {
		if (!this) { return };
		this.setState(state);
	}

	loadNetData = (page) => {
		let url = NetApi.newslist + '/uid/' + global.user.userData.id + '/style/5' + '/page/' + page;
        return this.netRequest.fetchGet(url, true)
            .then( result => {
                console.log(result);
                return result;
            })
            .catch( error => {
                consoleLog('链接服务器出错，请稍后重试', error);
                this.updateState({
                    ready: true,
                    error: true,
                    errorInfo: error
                })
            })
	}

	dropLoadMore = async () => {
		//如果是正在加载中或没有更多数据了，则返回
        if (this.state.showFoot != 0) {
            return;
        }
        if ((this.page != 1) && (this.page >= this.totalPage)) {
            return;
        } else {
            this.page++;
        }
        this.updateState({
            showFoot: 2
        })
        let result = await this.loadNetData(this.page);
        // console.log(this.totalPage);
        this.totalPage = result.data.pageSize;
        // console.log(result);
        let foot = 0;
        if (this.page >= this.totalPage) {
            // console.log(this.totalPage);
            foot = 1; //listView底部显示没有更多数据了
        }
        this.updateState({
            showFoot: foot,
            companyListData: this.state.companyListData.concat(result.data.info)
        })
	}

	freshNetData = () => {
		InteractionManager.runAfterInteractions(async ()=>{
			let result = await this.loadNetData(0);
	        if (result && result.code == 1) {
	            this.page = 0;
	            this.updateState({
	                showFoot: 0
	            })
	            this.updateState({
	                companyListData: result.data.info
	            })
	        }
		})
        
    }

	toWebview = (title, id, link, compent) => {
		const { navigate } = this.props.navigation;
		navigate(compent, {
			title: title,
			id: id,
			link: link,
            onCallBack:()=>{
                this.freshNetData()
            }
		})
	}

	


	renderCompanyItem = ({item}) => {
		let url = NetApi.base+NetApi.imagedetail+'/id/'+item.id+'/isapp/2';
		// let url = 'https://3g.163.com/news/photoview/0001/2291223.html';
		return (
			<View style={styles.videolist}>
				<TouchableOpacity onPress={()=>{this.toWebview(item.title, item.id, url, 'PicWebDetail')}} style={styles.videoTop}>
					<Image source={{uri: item.logo}} style={styles.videoImg} />
					<View style={styles.picnum}>
						<Text style={styles.picnuma}>{item.img_count}图</Text>
					</View>					
				</TouchableOpacity>
				<Text style={styles.picTitle}>{item.title}</Text>
				<View style={[GlobalStyles.newsinfo,styles.videoBot]}>
					<Text style={GlobalStyles.newsdata}>{item.create_time}</Text>
					<View style={GlobalStyles.newsnumwrap}>
						<View style={GlobalStyles.newsnum}>
							<Image source={require('../../assets/images/icons/icon_dianjiliang.png')} style={GlobalStyles.viewimg} />
							<Text style={GlobalStyles.viewnum}>{item.read_num}</Text>
						</View>
						<TouchableOpacity onPress={()=>{this.onSharePress(item.shareData, item.id, item.is_collect)}} style={styles.newsmoreWrap}>
							<Image source={require('../../assets/images/icons/icon_more1.png')} style={GlobalStyles.newsmore} />
						</TouchableOpacity>
					</View>
				</View>
			</View>
		)
	}

	renderHeaderView = () => {
		return (
			<View style={styles.shopListViewTitle}>
				
			</View>
		)
	}

	renderFooterView = () => {
        return <FooterComponent status = {this.state.showFoot} />;
	}
	
	renderEmptyView = () => {
		return this.state.loadMore && <ActivityIndicatorItem />;
	}

	renderSeparator = () => {
		return <View style={GlobalStyles.horLine} />;
	}

	render(){
		const { ready, error, refreshing, companyListData } = this.state;
		return (
			<View style={styles.container}>
				{ready ?
					<FlatList
						style = {styles.shopListView}
						keyExtractor = { item => item.id}
						data = {companyListData}
						extraData = {this.state}
						renderItem = {(item) => this.renderCompanyItem(item)}
						onEndReachedThreshold = {0.1}
						onEndReached = {(info) => this.dropLoadMore(info)}
						onRefresh = {this.freshNetData}
						refreshing = {refreshing}
						ItemSeparatorComponent={this.renderSeparator}
						ListHeaderComponent = {this.renderHeaderView}
						ListFooterComponent = {this.renderFooterView}
						ListEmptyComponent = {this.renderEmptyView}
					/>
					: <ActivityIndicatorItem />
				}
				{this.state.showSharePop?
	                <View>
	                    <ShareUtils
	                        style={{position:'absolute'}}
	                        show={this.state.showSharePop}
	                        closeModal={(show) => {
	                            this.setState({
	                                showSharePop: show
	                            })
	                        }}
	                        title = {this.state.title}
	                        description = {this.state.description}
	                        thumbImage = {this.state.logo}
	                        webpageUrl = {this.state.webpageUrl}
	                        id = {this.state.id}
	                        hasRefresh = {this.state.hasRefresh}
	                        hasCollect = {this.state.hasCollect}
	                        isCollected = {this.state.isCollected}
	                        reloadNetData = {() => this.freshNetData()}
	                        {...this.props}
	                    />
	                </View>
	                : null
	            }
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	videolist:{
		width: GlobalStyles.width,
		// marginBottom: 10,
		backgroundColor:'#fff',
		borderBottomColor: GlobalStyles.bgColor,
		borderBottomWidth: 10

	},
	videoTop:{
		position:'relative',
		
	},
	videoImg:{
		width: GlobalStyles.width,
		height: GlobalStyles.width*420/750,
	},
	videoShadow:{
		position:'absolute',
		width:GlobalStyles.width,
		height:210,
		top:0,
		left:0,
		backgroundColor:'rgba(0,0,0,0.14)'
	},
	videoIco:{
		position:'absolute',
		width:40,
		height:40,
		left:GlobalStyles.width*0.5,
		top:210*0.5,
		marginLeft:-20,
		marginTop:-20
	},
	picnum: {
		position:'absolute',
		bottom: 10,
		right:15,
		backgroundColor:'rgba(0,0,0,.45)',
		paddingLeft: 12,
		paddingRight: 12,
		paddingTop: 3,
		paddingBottom: 3,
		borderRadius: 15,
	},
	picnuma:{
		color:'#fff',
		fontSize:12,

	},
	picTitle:{
		fontSize: 16,
		color: '#444',
		lineHeight: 16,
		paddingLeft: 15,
		paddingTop:12
	},
	videoBot:{
		height:15,
		paddingLeft:15,
		paddingRight: 15,
		marginBottom:10
	},
    backgroundVideo:{
        width:GlobalStyles.width,
        height:210,
        backgroundColor:'black'
    },
    loading:{
    	position:'absolute',
    	top:210/2,
    	left:GlobalStyles.width/2,
    	width: 36,
    	height: 36,
    	marginTop: -18,
    	marginLeft: -18
    },
    controlbar:{
    	position:'absolute',
    	width:GlobalStyles.width,
    	height:210,
    	top:0,
    	left:0
    },
    newsmoreWrap: {
		height: 40,
		position: 'relative',
		alignItems: 'center',
		justifyContent: 'center'
    },
});
