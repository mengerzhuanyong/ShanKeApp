/**
 * 掌上山科APP
 * http://www.sdust.edu.cn/
 * 山科技术团队倾情打造
 */

import React, {Component} from 'react'
import {
	Text,
	View,
	Image,
	TextInput,
	ScrollView,
	StyleSheet,
	TouchableOpacity
} from 'react-native'

import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'
import HTMLView from 'react-native-htmlview';

export default class Detail extends Component {

	constructor(props) {
		super(props);
		this.state =  {
			item: this.props.navigation.state.params.item,
			data:''
		}
		this.netRequest = new NetRequest();
	}

	componentDidMount(){
		this.loadNetData();
	}

	onBack = () => {
		this.props.navigation.goBack();
	}

	updateState= (state) => {
		if (!this) { return };
		this.setState(state);
	}

	loadNetData = () => {
		let url = NetApi.newsdetail+'/id/'+this.state.item.id+'/isapp/1';
        this.netRequest.fetchGet(url)
            .then( result => {
                this.updateState({
                    data: result.data
                })
                // console.log(result.data);
            })
            .catch( error => {
                consoleLog('链接服务器出错，请稍后重试', error);
            })
	}

	render(){
		const contentHtmls = this.state.data.content;
		console.log(contentHtmls);
		return (
			<ScrollView style={styles.container}>
				<NavigationBar
					title = {this.state.data.title}
					leftButton = {UtilsView.getLeftButton(() => this.onBack())}
				/>
				<Text>{this.state.data.title}</Text>
				<HTMLView
			        value={contentHtmls}
			        stylesheet={styles}
			    />
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: GlobalStyles.bgColor,
	},
});
