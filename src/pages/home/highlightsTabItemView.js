/**
 * 掌上山科APP
 * http://www.sdust.edu.cn/
 * 山科技术团队倾情打造
 */

import React, {Component} from 'react'
import {
	Text,
	View,
	Image,
	FlatList,
	TextInput,
	ScrollView,
	StyleSheet,
	TouchableOpacity,
	InteractionManager
} from 'react-native'
import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import NavigationBar from '../../components/common/NavigationBar'
import { toastShort, consoleLog } from '../../utils/utilsToast'
import ActivityIndicatorItem from '../../components/common/ActivityIndicatorItem'

import BannerView from '../../components/common/Banner'
import FooterComponent from '../../components/common/footerComponent'

export default class HighlightsTabItemView extends Component {
	constructor(props) {
		super(props);
		this.state =  {
			ready: false,
            showFoot: 0,
            error: false,
            errorInfo: "",
			loadMore: false,
			refreshing: false,
			companyListData: [],
		}
		this.netRequest = new NetRequest();
	}

    /**
     * 初始化状态
     * @type {Boolean}
     */
    page = -1;
    totalPage = 0;
    loadMore = false;
    refreshing = false;

	async componentDidMount(){
		await this.dropLoadMore();
		setTimeout(() => {
			this.updateState({
				ready: true,
            	showFoot: 0 // 控制foot， 0：隐藏footer  1：已加载完成,没有更多数据   2 ：显示加载中
			})
		},800)
	}

	updateState = (state) => {
		if (!this) { return };
		this.setState(state);
	}

	loadNetData = (page) => {
		let url = NetApi.newslist + '/style/1' + '/page/' + page;
        return this.netRequest.fetchGet(url, true)
            .then( result => {
                console.log(result);
                return result;
            })
            .catch( error => {
                consoleLog('链接服务器出错，请稍后重试', error);
                this.updateState({
                    ready: true,
                    error: true,
                    errorInfo: error
                })
            })
	}

	dropLoadMore = async () => {
		//如果是正在加载中或没有更多数据了，则返回
        if (this.state.showFoot != 0) {
            return;
        }
        if ((this.page != 1) && (this.page >= this.totalPage)) {
            return;
        } else {
            this.page++;
        }
        this.updateState({
            showFoot: 2
        })
        let result = await this.loadNetData(this.page);
        // console.log(this.totalPage);
        this.totalPage = result.data.pageSize;
        // console.log(result);
        let foot = 0;
        if (this.page >= this.totalPage) {
            // console.log(this.totalPage);
            foot = 1; //listView底部显示没有更多数据了
        }
        this.updateState({
            showFoot: foot,
            companyListData: this.state.companyListData.concat(result.data.info)
        })
	}

	freshNetData = () => {
		InteractionManager.runAfterInteractions(async ()=>{
	        let result = await this.loadNetData(0);
	        if (result && result.code == 1) {
	            this.page = 0;
	            this.updateState({
	                showFoot: 0
	            })
	            this.updateState({
	                companyListData: result.data.info
	            })
	        }
        })
    }

	toWebview = (title, id, link, compent) => {
		const { navigate } = this.props.navigation;
		navigate(compent, {
			title: title,
			id: id,
			link: link,
		})
	}

	renderCompanyItem = ({item}) => {
		let url = NetApi.base+NetApi.newsdetail+'/id/'+item.id+'/isapp/2';
		return (
			<TouchableOpacity onPress={() => {this.toWebview(item.title, item.id, url, 'NewsWebDetail')}} style={styles.newslist}>
				<View style={styles.newsleft}>
					<Text style={styles.newstitle}>{item.title}</Text>
					<View style={GlobalStyles.newsinfo}>
						<Text style={GlobalStyles.newsdata}>{item.create_time}</Text>
						<View style={GlobalStyles.newsnum}>
							<Image source={require('../../assets/images/icons/icon_dianjiliang.png')} style={GlobalStyles.viewimg} />
							<Text style={GlobalStyles.viewnum}>{item.read_num}</Text>
						</View>
					</View>
				</View>
				<View style={styles.newsright}>
					<Image source={{uri: item.logo ? item.logo : 'http://app.sdust.edu.cn/defaultimg.jpg'}} style={styles.rightimg} />
				</View>
			</TouchableOpacity>
		)
	}

	renderHeaderView = () => {
		return (
			<View style={styles.shopListViewTitle}>
				<BannerView {...this.props} />
			</View>
		)
	}

	renderFooterView = () => {
        return <FooterComponent status = {this.state.showFoot} />;
	}
	
	renderEmptyView = () => {
		return this.state.loadMore && <ActivityIndicatorItem />;
	}

	renderSeparator = () => {
		return <View style={GlobalStyles.horLine} />;
	}

	render(){
		const { ready, error, refreshing, companyListData } = this.state;
		return (
			<View style={styles.container}>
				{ready ?
					<FlatList
						style = {styles.shopListView}
						keyExtractor = { item => item.id}
						data = {companyListData}
						extraData = {this.state}
						renderItem = {(item) => this.renderCompanyItem(item)}
						onEndReachedThreshold = {0.1}
						onEndReached = {(info) => this.dropLoadMore(info)}
						onRefresh = {this.freshNetData}
						refreshing = {refreshing}
						ItemSeparatorComponent={this.renderSeparator}
						ListHeaderComponent = {this.renderHeaderView}
						ListFooterComponent = {this.renderFooterView}
						ListEmptyComponent = {this.renderEmptyView}
					/>
					: <ActivityIndicatorItem />
				}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	newslist:{
		backgroundColor:'#fff',
		borderBottomColor:'#eeeff3',
		borderBottomWidth:1,
		paddingTop:15,
		paddingBottom:15,
		paddingLeft:10,
		paddingRight:10,
		display:'flex',
		flexDirection:'row',
		justifyContent:'space-between',
		alignItems:'center',
	},
	newsleft:{
		width: (GlobalStyles.width-20)*0.6,
		marginRight: (GlobalStyles.width-20)*0.04,
	},
	newstitle:{
		fontSize: 16,
		color: '#444',
		lineHeight: 22,

	},
	newsright:{
		width: (GlobalStyles.width-20)*0.36,
	},
	rightimg:{
		width: (GlobalStyles.width-20)*0.36,
		height: (GlobalStyles.width-20)*0.36*420/750,
	},
});
