/**
 * 掌上山科APP
 * http://www.sdust.edu.cn/
 * 山科技术团队倾情打造
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    FlatList,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    Alert,
    ActivityIndicator, 
    Slider
} from 'react-native'
import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import { toastShort, consoleLog } from '../../utils/utilsToast'
import ActivityIndicatorItem from '../../components/common/ActivityIndicatorItem'
import CBYImage from "../../components/common/CBYImage";

import FooterComponent from '../../components/common/footerComponent'
const Sound = require("react-native-sound");
let timer = null;

export default class MusicTabItemView extends Component {
	constructor(props) {
		super(props);
		this.state =  {
			ready: false,
            showFoot: 0,
            error: false,
            errorInfo: "",
			loadMore: false,
			refreshing: false,
			companyListData: [],
		}
		this.netRequest = new NetRequest();
		this.isPlaying = global.currentSound ? global.currentSound.isPlaying : false;
		this.sliders = [];
		this.buttons = [];
		this.progressTimes = [];
		// this.totalProgressTimes = [];

	}

    /**
     * 初始化状态
     * @type {Boolean}
     */
    page = -1;
    totalPage = 0;
    loadMore = false;
    refreshing = false;

	async componentDidMount(){
		await this.dropLoadMore();
		setTimeout(() => {
			this.updateState({
				ready: true,
            	showFoot: 0 // 控制foot， 0：隐藏footer  1：已加载完成,没有更多数据   2 ：显示加载中
			})
		},0)
	}

	updateState = (state) => {
		if (!this) { return };
		this.setState(state);
	}

	loadNetData = (page) => {
		let url = NetApi.newslist + '/style/4' + '/page/' + page;
        return this.netRequest.fetchGet(url, true)
            .then( result => {
                console.log(result);
                return result;
            })
            .catch( error => {
                consoleLog('链接服务器出错，请稍后重试', error);
                this.updateState({
                    ready: true,
                    error: true,
                    errorInfo: error
                })
            })
	}

	dropLoadMore = async () => {
		//如果是正在加载中或没有更多数据了，则返回
        if (this.state.showFoot != 0) {
            return;
        }
        if ((this.page != 1) && (this.page >= this.totalPage)) {
            return;
        } else {
            this.page++;
        }
        this.updateState({
            showFoot: 2
        })
        let result = await this.loadNetData(this.page);
        // console.log(this.totalPage);
        this.totalPage = result.data.pageSize;
        // console.log(result);
        let foot = 0;
        if (this.page >= this.totalPage) {
            // console.log(this.totalPage);
            foot = 1; //listView底部显示没有更多数据了
        }
        this.updateState({
            showFoot: foot,
            companyListData: this.state.companyListData.concat(result.data.info)
        })
	}

	freshNetData = async () => {

        let result = await this.loadNetData(0);
        if (result && result.code == 1) {
            this.page = 0;
            this.updateState({
                showFoot: 0
            })
            this.updateState({
                companyListData: result.data.info
            })
        }
    }


	play(){

		if(global.currentIndex == global.lastIndex){
		    let button = this.buttons[global.currentIndex];
				if(global.currentSound && global.currentSound.isPlaying && global.currentSound.paused){
					global.currentSound.play((success)=>{});
                    global.currentSound.paused = false;
                    button.setNativeProps({
                        source:require("../../assets/images/icons/icon_music_zanting.png")
                    })
				}else{
					if(global.currentSound && global.currentSound.isPlaying){
						global.currentSound.pause();
						global.currentSound.paused = true;
                        button.setNativeProps({
                            source:require("../../assets/images/icons/icon_music_bofang.png")
                        })
					}
				}
		}else{
            global.currentSound && global.currentSound.release();
			global.currentSound = null;

            let currentProgressTextView = this.progressTimes[global.lastIndex];
            if(currentProgressTextView){
                currentProgressTextView.setNativeProps({
                    text:"00:00"
                });
			}

            let lastSlider = this.sliders[global.lastIndex];
            lastSlider && lastSlider.setNativeProps({
                value:-1,
                disabled:true
            });

            let lastButton = this.buttons[global.lastIndex];
            lastButton && lastButton.setNativeProps({
                source:require("../../assets/images/icons/icon_music_bofang.png")
            });


            this.slider = this.sliders[global.currentIndex];
            if(this.slider){
                this.slider.setNativeProps({
                    disabled:false
                });
            }

            let button = this.buttons[global.currentIndex];
            button.setNativeProps({
                source:require("../../assets/images/icons/icon_music_zanting.png")
            })

            let item = this.state.companyListData[global.currentIndex];
            global.currentSound = new Sound(item.url,null,(error)=>{
					if(error){

					}else {
                        global.currentSound.setNumberOfLoops(-1);
                        global.currentSound.play((success)=>{

                        });
                       // global.lastIndex = global.currentIndex;
					}
			});

               global.lastIndex = global.currentIndex;
        }

        this.initTimer();
	}


    timeFormate(time){
        let second = Math.floor(Math.ceil(time) % 60);
        let minute = Math.floor(Math.ceil(time) / 60 % 60);
        let hour = Math.floor(Math.ceil(time) / 3600);
        second = second > 0 ? second : 0;
        minute = minute > 0 ? minute : 0;
        hour = hour > 0 ? hour : 0;
        if(hour > 0){
            if(hour < 10){
                hour = "0"+hour;
            }
        }

        if(minute >= 0){
            if(minute < 10){
                minute = "0"+minute
            }
        }

        if(second >= 0){
            if(second < 10){
                second = "0" + second;
            }
        }

        if(hour > 0){
            return hour+":"+minute+":"+second;
        }
        return minute + ":" + second;

    }

    initTimer(){
        timer = setInterval(()=>{
            if(global.currentSound){

            	let currentProgressTextView = this.progressTimes[global.currentIndex];
            	// let totalProgressTextView = this.totalProgressTimes[global.currentIndex];

                this.totalProgress = global.currentSound.getDuration();
                this.setState({
                    totalProgress:global.currentSound.getDuration()
                });

    			//  totalProgressTextView.setNativeProps({
				// 	text:this.timeFormate(this.totalProgress)
				// });

                this.slider && this.slider.setNativeProps({
                    maximumValue:this.totalProgress
                })
                global.currentSound.getCurrentTime((seconds)=>{
                    this.slider && this.slider.setNativeProps({
                        value:seconds
                    })
					// console.log(seconds);
                    currentProgressTextView.setNativeProps({
						text:this.timeFormate(seconds)
					})
					// currentProgressTextView.setValue(""+seconds+"");
                    if(seconds >= this.totalProgress - 1){
                        this.playNext();
                    }
                })

            }
        },1000);
    }


    playNext(){
        if(global.currentIndex < this.state.companyListData.length-1){
            global.currentIndex += 1;
            this.play();
        }else{
            global.currentIndex = 0;
            this.play();
        }
    }


	renderCompanyItem = ({item}) => {
		return (
			<View style={styles.musiclist}>
				<View style={styles.musicleft} >
					<TouchableOpacity  activeOpacity={0.9} onPress={()=>{
						global.currentIndex = this.state.companyListData.indexOf(item);
						this.play();
					}}>
						<CBYImage ref={e=>this.buttons[this.state.companyListData.indexOf(item)] = e} source={require('../../assets/images/icons/icon_music_bofang.png')} style={styles.playicon} />
					</TouchableOpacity>
				</View>
				<View style={styles.musicright} >
					<View style={styles.musictop} >
						<Text style={styles.musicTitle}>{item.title}</Text>
					</View>
					<View style={styles.musicbot} >
						<TextInput ref={e => this.progressTimes[this.state.companyListData.indexOf(item)] = e} editable={false} style={styles.musictime} defaultValue={"00:00"}/>
						<View style={styles.musicprogress}>
                            <Slider
                                ref={e=> this.sliders[this.state.companyListData.indexOf(item)] = e}
                                maximumTrackTintColor={"#dddddd"}
                                minimumTrackTintColor={"#2E77E5"}
                                onValueChange={(value) => {
                                    if(global.currentSound){
                                        global.currentSound.setCurrentTime(value)
                                        this.slider && this.slider.setNativeProps({
                                            value:value
                                        })
                                    }
                                }}
                                disabled={true}
                                maximumValue={0}
                                value={0}
                            />
                        </View>
						<Text style={[styles.musictime, {textAlign: 'right'}]}>{item.hour}</Text>
					</View>
				</View>
			</View>
		)
	}
	// 结束时间自动获取
	// <TextInput ref={e => this.totalProgressTimes[this.state.companyListData.indexOf(item)] = e} style={styles.musictime} defaultValue={"00:00"}/>
	renderHeaderView = () => {
		return (
			<View style={styles.shopListViewTitle}>
				
			</View>
		)
	}

	renderFooterView = () => {
        return <FooterComponent status = {this.state.showFoot} />;
	}
	
	renderEmptyView = () => {
		return this.state.loadMore && <ActivityIndicatorItem />;
	}

	renderSeparator = () => {
		return <View style={GlobalStyles.horLine} />;
	}

	render(){
		const { ready, error, refreshing, companyListData } = this.state;
		return (
			<View style={styles.container}>
				{ready ?
					<FlatList
						style = {styles.shopListView}
						keyExtractor = { item => item.id}
						data = {companyListData}
						extraData = {this.state}
						renderItem = {(item) => this.renderCompanyItem(item)}
						onEndReachedThreshold = {0.1}
						onEndReached = {(info) => this.dropLoadMore(info)}
						onRefresh = {this.freshNetData}
						refreshing = {refreshing}
						ItemSeparatorComponent={this.renderSeparator}
						ListHeaderComponent = {this.renderHeaderView}
						ListFooterComponent = {this.renderFooterView}
						ListEmptyComponent = {this.renderEmptyView}
					/>
					: <ActivityIndicatorItem />
				}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	musiclist:{
		backgroundColor: '#fff',
		paddingTop: 12,
		paddingBottom: 10,
		paddingLeft: 15,
		paddingRight: 15,
		borderBottomColor: '#f2f2f2',
		borderBottomWidth: 1,
		display: 'flex',
		flexDirection: 'row',
		justifyContent:'center',
		alignItems:'center'
	},
	musicleft:{
		width:35,
		// backgroundColor: '#f00'
	},
	musicico:{
		width: 20,
		height: 20,
	},
	musicnum:{
		fontSize: 15,
		color: '#989898',
		lineHeight: 20
	},
	musicright:{
		flex: 1,
	},
	musictop:{
		display: 'flex',
		flexDirection:'row',
		justifyContent:'space-between',
		alignItems:'center'
	},
	musicTitle:{
		fontSize: 16,
		color: '#444',
	},
	musicbot:{
		display:'flex',
		flexDirection:'row',
		justifyContent:'space-around',
		alignItems:'center',
		paddingTop: 6,
		// backgroundColor: '#f00'
	},
	musictime:{
		color:'#444',
		fontSize:14,
		width: 45,
		textAlign: 'left'
	},
	musicprogress:{
		flex: 1,
		borderRadius:3,
		// height:3,
		// backgroundColor:'#ddd',

	},
	playicon: {
		width: 28,
		height: 28,
		marginRight: 5
	}
});
