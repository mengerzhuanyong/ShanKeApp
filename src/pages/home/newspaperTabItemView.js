/**
 * 速芽物流 - 公共详情页
 * http://www.sdust.edu.cn/
 * @Meng
 */

import React, { Component } from 'react';
import {
	View,
	Text,
	Image,
	WebView,
	Dimensions,
	StyleSheet,
	StatusBar,
	TouchableOpacity
} from 'react-native';

import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'
import *as wechat from 'react-native-wechat'
import ShareUtils from '../../components/common/shareUtils'
const WEBVIEW_REF = 'webview';

export default class WebViewPage extends Component {

	constructor(props){
		super(props);
		const { params } = this.props.navigation.state;
		this.state={
			url: '',
			title: '',
			id: 0,
            showSharePop: false,
            description: '',
            webpageUrl: '',
            logo: '',
            backButtonEnabled: false,
		    forwardButtonEnabled: false,
		    loading: true,
            hasRefresh: true,
            hasCollect: false,
            isCollected: 0,
            hasgoBack: false
		}
		this.netRequest = new NetRequest();
	}

	componentDidMount(){
		this.loadNetData();
	}

	onSharePress = (id) => {
		this.setState({
            showSharePop: !this.state.showSharePop,
            id: id
        });
        
        // let url = NetApi.paper + '/id/' + id + '/isapp/1';
        // this.netRequest.fetchGet(url, true)
        //     .then( result => {
        //     	console.log(result.data);
        //     	this.setState({
        //     		description: '《山东科大报》是山东科技大学党委机关报，在校党委及其职能部门党委宣传部的领导下开展舆论宣传工作。',
        //     		webpageUrl: result.data[0].url + '/isapp/0',
        //     		logo: NetApi.root + 'logo120.png',
        //     		title: '《山东科大报》第' + result.data[0].period + '期',
        //     	});
        //     })
        //     .catch( error => {
        //         console.log('获取信息失败', error);
        //     })
	}

	updateState= (state) => {
		if (!this) { return };
		this.setState(state);
	}

	

	onClose = () => {
		let message = 'home'
		this.webview.postMessage(message);
	}

	loadNetData = () => {
		let url = NetApi.paper;
        this.netRequest.fetchGet(url, true)
            .then( result => {
            	console.log(result.data);
            	this.setState({
            		url: result.data[0].url,
            		id: result.data[0].id
            	});
            })
            .catch( error => {
                console.log('获取信息失败', error);
            })
	}

	// 报纸详情页面在收藏操作之后重新渲染一遍页面
	loadNetDataPaper = () => {
		let url = NetApi.newsdetail + '/id/' + this.state.id + '/uid/' + global.user.userData.id + '/isapp/1';
        this.netRequest.fetchGet(url, true)
            .then( result => {
            	console.log(result.data);
            	this.setState({
            		description: result.data.describe,
            		webpageUrl: NetApi.base + NetApi.newsdetail + '/id/' + this.state.id + '/isapp/0',
            		logo: result.data.logo,
            		title: result.data.title,
            		isCollected: result.data.is_collect,
            	});
            })
            .catch( error => {
                console.log('获取信息失败', error);
            })
	}

	goBack = () => {
		this.webview.goBack();
	};

	getCurrentPage = (event) => {
		console.log(event);
		let thisurl = event.url;
		console.log(thisurl);
		if(this.state.url == event.url) {
			this.setState({
				hasgoBack: false
			})
		}else {
			this.setState({
				hasgoBack: true
			})
		}
		if(thisurl.indexOf('isapp/2')>-1){
			thisurl = thisurl.replace('isapp/2','isapp/0');
			// 获取文章id
			thisurl1 = thisurl.split('id/');
			thisurl2 = thisurl1[1].split('/isapp');
			this.setState({
				hasCollect: true,
				id: thisurl2[0]
			})		

			//获取报纸中文章的收藏状态
			let collectUrl = NetApi.paperCollect;
			let data = {
	            aid: thisurl2[0],
	            uid: global.user.userData.id,
	        }
	        
	        this.netRequest.fetchPost(collectUrl, data, true)
	            .then( result => {
	                console.log(result);
	                if (result && result.code == 1) {
	                    this.setState({
							isCollected: result.data
						})	
	                }
	            })
	            .catch( error => {
	                console.log(error)
	            })
			
		}else{
			this.setState({
				hasCollect: false
			})
		}
		console.log(thisurl);
		this.setState({
			description: '《山东科大报》是山东科技大学党委机关报，在校党委及其职能部门党委宣传部的领导下开展舆论宣传工作。',
			webpageUrl: thisurl,
			logo: NetApi.root + 'logo120.png',
			title: event.title,
			backButtonEnabled: event.canGoBack,
	        forwardButtonEnabled: event.canGoForward,
	        loading: event.loading,
		});
	}



	render() {
		return (
			<View style={styles.container}>
				<View style={styles.paperTop}>
					{this.state.hasgoBack ? <TouchableOpacity onPress={()=>{this.goBack()}} style={styles.paperbackWrap}>
						<Image source={require('../../assets/images/icons/icon_paper_back2.png')} style={styles.paperback} />
					</TouchableOpacity> : null}
					<TouchableOpacity onPress={()=>{this.onClose()}} style={styles.papercloseWrap} style={{display: 'none'}}>
						<Image source={require('../../assets/images/icons/icon_paper_close.png')} style={styles.paperclose} />
					</TouchableOpacity>
					<Text style={styles.paperTopTitle}>山东科大报</Text>
					<TouchableOpacity onPress={()=>{this.onSharePress(this.state.id)}} style={styles.papermoreWrap}>
						<Image source={require('../../assets/images/icons/icon_share.png')} style={styles.papershare} />
					</TouchableOpacity>
				</View>
				<WebView
					ref={(webView) => {this.webview = webView}}
					startInLoadingState={true}
					source={{uri: this.state.url}}
					style={styles.webContainer}
					onNavigationStateChange={(event)=>{this.getCurrentPage(event)}}
					renderLoading={() => {
                        return <View style={GlobalStyles.loadingWrap}><Image source={require('../../assets/images/icons/loading.gif')} style={GlobalStyles.loadingIco} /></View>
                    }}
				/>
				{this.state.showSharePop?
	                <View>
	                    <ShareUtils
	                        style={{position:'absolute'}}
	                        show={this.state.showSharePop}
	                        closeModal={(show) => {
	                            this.setState({
	                                showSharePop: show
	                            })
	                        }}
	                        title = {this.state.title}
	                        description = {this.state.description}
	                        thumbImage = {this.state.logo}
	                        webpageUrl = {this.state.webpageUrl}
	                        id = {this.state.id}
	                        webview = {this.webview}
	                        hasRefresh = {this.state.hasRefresh}
	                        hasCollect = {this.state.hasCollect}
	                        isCollected = {this.state.isCollected}
	                        reloadNetData = {() => this.loadNetDataPaper()}
	                        {...this.props}
	                    />
	                </View>
	                : null
	            }				
			</View>
		);
	}

}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	webContainer: {
		flex: 1,
		backgroundColor: '#f1f2f3',
		width: GlobalStyles.width
	},
	paperTop: {
		height: 50,
		flexDirection: 'row',
		justifyContent: 'center',
		position: 'relative',
		backgroundColor: '#fff',
		alignItems: 'center',
	},
	paperTopTitle: {
		color: '#666',
		fontSize: 18,
		fontWeight: 'bold',
	},
	papermoreWrap: {
		position: 'absolute',
		height: 50,
		top: 0,
		right: 15,
		alignItems: 'center',
		justifyContent: 'center'
	},
	papershare: {
		width: 24,
		height: 24
	},
	paperbackWrap: {
		position: 'absolute',
		height: 50,
		top: 0,
		left: 15,
		alignItems: 'center',
		justifyContent: 'center'
	},
	paperback: {
		width: 24,
		height: 24
	},
	papercloseWrap: {
		position: 'absolute',
		height: 50,
		top: 0,
		left: 50,
		alignItems: 'center',
		justifyContent: 'center'
	},
	paperclose: {
		width: 22,
		height: 22
	},
});