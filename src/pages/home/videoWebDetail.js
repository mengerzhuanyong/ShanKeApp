/**
 * 速芽物流 - 公共详情页
 * http://www.sdust.edu.cn/
 * @Meng
 */

import React, { Component } from 'react';
import {
	View,
	Text,
	Image,
	WebView,
	Dimensions,
	StyleSheet,
	StatusBar,
	TouchableOpacity
} from 'react-native';
import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'
import *as wechat from 'react-native-wechat'
import ShareUtils from '../../components/common/shareUtils'

const WEBVIEW_REF = 'webview';

export default class VideoWebDetail extends Component {

	constructor(props){
		super(props);
		const { params } = this.props.navigation.state;
		this.state={
			url: params.link,
			title: params.title,
			id: params.id,
            showSharePop: false,
            description: '',
            webpageUrl: '',
            logo: '',
            backButtonEnabled: false,
		    forwardButtonEnabled: false,
		    loading: true,
            hasRefresh: true,
            hasCollect: true,
            isCollected: 0,
		}
		this.netRequest = new NetRequest();
	}

	componentDidMount(){
		this.loadNetData();
	}

	onBack = () => {
		if(this.state.backButtonEnabled){
	    	this.webview.goBack();
		}else{
			this.props.navigation.goBack();
        	this.props.navigation.state.params.onCallBack();
		}
	}

    componentWillUnmount(){
        this.onBack();
        
    }

	onSharePress = () => {
		this.setState({
            showSharePop: !this.state.showSharePop
        })
	}

	updateState= (state) => {
		if (!this) { return };
		this.setState(state);
	}

	loadNetData = () => {
		let url = NetApi.newsdetail + '/id/' + this.state.id + '/uid/' + global.user.userData.id + '/isapp/1';
        this.netRequest.fetchGet(url, true)
            .then( result => {
            	console.log(result.data);
            	this.setState({
            		description: result.data.describe,
            		webpageUrl: NetApi.base + NetApi.newsdetail + '/id/' + this.state.id + '/isapp/0',
            		logo: NetApi.root + 'logo120.png',
            		title: result.data.title,
            		isCollected: result.data.is_collect,
            	});
            })
            .catch( error => {
                console.log('获取信息失败', error);
            })
	}

	getCurrentPage = (event) => {
		console.log(event);
		let thisurl = event.url;
		
		this.setState({
    		backButtonEnabled: event.canGoBack,
	        forwardButtonEnabled: event.canGoForward,
	        loading: event.loading,
    	});
	}

	render() {
		let navigationBar = {
			backgroundColor: '#fff',
			borderBottomColor: '#f2f2f2',
			borderBottomWidth: 1,
		};
		return (
			<View style={styles.container}>
				<NavigationBar
					title = '文章详情'
					style = {navigationBar}
					leftButton = {UtilsView.getLeftBlackButton(() => this.onBack())}
					rightButton = {UtilsView.getRightBlackButton(() => this.onSharePress())}
				/>
				<StatusBar
					animated = {true}
					hidden = {false}
					backgroundColor = {'#fff'}
					translucent = {true}
					barStyle ={'dark-content'}
				/>
				<WebView
					ref={(webView) => {this.webview = webView}}
					startInLoadingState={true}
					source={{uri: this.state.url}}
					onNavigationStateChange={(event)=>{this.getCurrentPage(event)}}
					renderLoading={() => {
                        return <View style={GlobalStyles.loadingWrap}><Image source={require('../../assets/images/icons/loading.gif')} style={GlobalStyles.loadingIco} /></View>
                    }}
					style={styles.webContainer}
					mediaPlaybackRequiresUserAction={true}
				/>
				{this.state.showSharePop?
	                <View>
	                    <ShareUtils
	                        style={{position:'absolute'}}
	                        show={this.state.showSharePop}
	                        closeModal={(show) => {
	                            this.setState({
	                                showSharePop: show
	                            })
	                        }}
	                        title = {this.state.title}
	                        description = {this.state.description}
	                        thumbImage = {this.state.logo}
	                        webpageUrl = {this.state.webpageUrl}
	                        id = {this.state.id}
	                        webview = {this.webview}
	                        hasRefresh = {this.state.hasRefresh}
	                        hasCollect = {this.state.hasCollect}
	                        isCollected = {this.state.isCollected}
	                        reloadNetData = {() => this.loadNetData()}
	                        {...this.props}
	                    />
	                </View>
	                : null
	            }
			</View>
		);
	}

}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},	
	webContainer: {
		flex: 1,
		backgroundColor: '#fff',
	},
});