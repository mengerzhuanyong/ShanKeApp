/**
 * 掌上山科APP
 * http://www.sdust.edu.cn/
 * 山科技术团队倾情打造
 */

import React, {Component} from 'react'
import {
	Text,
	View,
	Image,
	Linking,
	Platform,
	TextInput,
	ScrollView,
	StyleSheet,
	TouchableOpacity,
} from 'react-native'

import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import { Constants } from '../../constants/Constants'
import GlobalStyles from '../../constants/GlobalStyles'
import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'
import NewsTabItemView from './newsTabItemView'
import HighlightsTabItemView from './highlightsTabItemView'
import MusicTabItemView from './musicTabItemView'
import VideoTabItemView from './videoTabItemView'
import MovieTabItemView from './movieTabItemView'
import PicTabItemView from './picTabItemView'
import NoticeTabItemView from './noticeTabItemView'
import NewspaperTabItemView from './newspaperTabItemView'
import SegmentedView from '../../components/segmented/SegmentedView'
import AlertManager from '../../components/common/AlertManager'
// import {SegmentedView, Label} from 'teaset';
// import ScrollableTabView, {ScrollableTabBar, DefaultTabBar} from 'react-native-scrollable-tab-view';
// import DefaultTabBar from '../../components/common/defaultTabBar'
					
export default class Index extends Component {

	constructor(props) {
		super(props);
		this.state =  {
			ready: false,
			navArray: [
				{type: 1, title: '快讯'},
				{type: 2, title: '动态'},
				{type: 3, title: '影像'},
				{type: 4, title: '视频'},
				{type: 5, title: '图说'},
				{type: 6, title: '读报'},
				{type: 7, title: '音频'},
				{type: 8, title: '通告'},
			]
		}
		this.netRequest = new NetRequest();
	}

	componentDidMount(){
		this.requestNavArray();
		this.checkUpdateVersion();
	}

	onBack = () => {
		this.props.navigation.goBack();
	}

	toUser = () => {
		const { navigate } = this.props.navigation;
		navigate('User', {
			
		})
	}

	updateState= (state) => {
		if (!this) { return };
		this.setState(state);
	}

	requestNavArray = async () => {
		let url = NetApi.system_config;
		try {
			let result = await this.netRequest.fetchGet(url, true);
			if (result.code === 1) {
				this.setState({
					ready: true,
					navArray: result.data.home_nav_array,
				});
			} else {
				this.setState({
					ready: true
				})
			}
		} catch (e) {
			this.setState({
				ready: true
			});
			// console.log('e---->', e);
		}
	};

	checkUpdateVersion = () => {
        console.log(global.user.loginState);
        let url = NetApi.user + '/id/' + global.user.userData.id;
        this.netRequest.fetchGet(url, true)
            .then( result => {
            	console.log(result.data);
				if(Platform.OS === 'ios') {
					if(result.data.version.ios != Constants.VERSION_NAME && result.data.is_online == 1){
                        const params = {
                            title: '软件升级',
                            detail: '系统检测到当前应用有新版本，不升级可能导致软件部分功能不可用，请立即升级！',
                            actions: [
                                { title: '暂不', titleStyle: {  }, actionStyle: {  }, onPress: () => { 
                                    
                                } },
                                { title: '立即升级', onPress: () => { 
                                    Linking.openURL(result.data.version.ios_url);
                                } }
                            ]
                        }
                        AlertManager.show(params)
                    }
				}else {
					if(result.data.version.an != Constants.VERSION_NAME){
                        const params = {
                            title: '软件升级',
                            detail: '系统检测到当前应用有新版本，不升级可能导致软件部分功能不可用，请立即升级！',
                            actions: [
                                { title: '暂不', titleStyle: {  }, actionStyle: {  }, onPress: () => { 
                                    
                                } },
                                { title: '立即升级', onPress: () => { 
                                    Linking.openURL(result.data.version.an_url);
                                } }
                            ]
                        }
                        AlertManager.show(params)
                    }
				}  
            })
            .catch( error => {
                // console.log('获取信息失败', error);
            })
	}


	_renderNavArrayContent = (navArray) => {
		if (!navArray || navArray.length < 1) return null;
		let content = navArray.map((item, index) => {
			let component = null;
			switch(item.type) {
				case 1:
					component = <HighlightsTabItemView {...this.props}/>;
					break;
				case 2:
					component = <NewsTabItemView {...this.props}/>;
					break;
				case 3:
					component = <MovieTabItemView {...this.props}/>;
					break;
				case 4:
					component = <VideoTabItemView {...this.props}/>;
					break;
				case 5:
					component = <PicTabItemView {...this.props}/>;
					break;
				case 6:
					component = <NewspaperTabItemView {...this.props}/>;
					break;
				case 7:
					component = <MusicTabItemView {...this.props}/>;
					break;
				case 8:
					component = <NoticeTabItemView {...this.props}/>;
					break;
				default:
					break;
			}
			return (
				<View
					key={index}
			  		title={item.title}
			  		titleStyle={{color: '#666', fontSize: 15, }}
			  	  	itemStyle={{paddingLeft: 10, paddingRight: 10}}
			  		activeTitleStyle={{color: GlobalStyles.themeColor, fontSize: 15, fontWeight: 'bold', }}
			  		style={{backgroundColor: 'transparent', flex: 1}}
			  	>
			      	{component}
			  	</View>
			);
		});
		return content;
	};


	render(){
		let {ready, navArray} = this.state;
		// console.log('navArray---->', navArray);
		return (
			<View style={styles.container}>
				<NavigationBar
					title = {'掌上山科'}
					// leftButton = {UtilsView.getLeftUserButton(() => this.toUser())}
				/>
				{ready && navArray.length > 0 ?
				    <SegmentedView 
					    style={{flex: 1,  }} 
					    justifyItem={'scrollable'} 
					    barStyle={{height: 45, borderBottomColor: '#f2f2f2', borderBottomWidth: 1}} 
					    indicatorPositionPadding={0}
				    >
				    	{this._renderNavArrayContent(navArray)}
					</SegmentedView>
					: 
					<View style={{flex: 1, backgroundColor: '#fff'}} />
				}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: GlobalStyles.bgColor,
	},
});