/**
 * 掌上山科APP
 * http://www.sdust.edu.cn/
 * 山科技术团队倾情打造
 */

import React, {Component} from 'react'
import {
	Text,
	View,
	Image,
	TextInput,
	ScrollView,
	StyleSheet,
	TouchableOpacity,
} from 'react-native'


import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'
import NewsTabItemView from './newsTabItemView'
import HighlightsTabItemView from './highlightsTabItemView'
import MusicTabItemView from './musicTabItemView'
import VideoTabItemView from './videoTabItemView'
import MovieTabItemView from './movieTabItemView'
import PicTabItemView from './picTabItemView'
import NewspaperTabItemView from './newspaperTabItemView'

import ScrollableTabView, {ScrollableTabBar, DefaultTabBar} from 'react-native-scrollable-tab-view';
// import DefaultTabBar from '../../components/common/defaultTabBar'
					
export default class Index extends Component {

	constructor(props) {
		super(props);
		this.state =  {}
		this.netRequest = new NetRequest();
	}

	componentDidMount(){
		this.loadNetData();
		// setTimeout(() => {
		// 	this.scrollableTabView.goToPage(0)
		// },300)
	}

	onBack = () => {
		this.props.navigation.goBack();
	}

	toUser = () => {
		const { navigate } = this.props.navigation;
		navigate('User', {
			
		})
	}

	updateState= (state) => {
		if (!this) { return };
		this.setState(state);
	}

	loadNetData = () => {
        console.log(global.user.loginState);
	}

	// leftButton = {UtilsView.getLeftUserButton(() => this.toUser())}

	render(){
		return (
			<View style={styles.container}>
				<NavigationBar
					title = {'掌上山科'}
				/>
				<ScrollableTabView
					renderTabBar={() => <DefaultTabBar/>}
					initialPage={0}
					ref = {(ref) => {this.scrollableTabView = ref; }}
					tabBarUnderlineStyle={{backgroundColor: GlobalStyles.themeColor, height: 2, marginTop: -9, paddingTop: 0}}
					tabBarBackgroundColor='#fff'
					tabBarActiveTextColor='#0068B7'
					tabBarInactiveTextColor='#666'
					tabBarTextStyle={{fontSize: 15}}
					tabBarPosition='overlayTop'
				>
			        <HighlightsTabItemView {...this.props} tabLabel="快讯" />
			        <NewsTabItemView {...this.props} tabLabel="动态" />
			        <VideoTabItemView {...this.props} tabLabel="视频" />
			        <MovieTabItemView {...this.props} tabLabel="影像" />
			        <PicTabItemView {...this.props} tabLabel="图说" />
			        <NewspaperTabItemView {...this.props} tabLabel="读报" />
			        <MusicTabItemView {...this.props} tabLabel="音频" />
			    </ScrollableTabView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: GlobalStyles.bgColor,
	},
});