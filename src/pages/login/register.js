/**
 * 掌上山科APP
 * http://www.sdust.edu.cn/
 * 山科技术团队倾情打造
 */

import React, {Component} from 'react'
import {
	Text,
	View,
	Image,
	TextInput,
	ScrollView,
	StyleSheet,
	TouchableOpacity
} from 'react-native'

import { NavigationActions } from 'react-navigation'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'

import SendSMS from '../../components/common/sendSMS'

export default class Register extends Component {

	constructor(props) {
		super(props);
		this.state =  {
            user: '',
            seconds: 60,
            mobile:'',
            passwd:'',
            passwdd:'',
            code:'',
            codeAlreadySend: false,
		}
		this.netRequest = new NetRequest();
	}

	componentDidMount(){
		this.loadNetData();
	}

	onBack = () => {
		this.props.navigation.goBack();
	}

	updateState= (state) => {
		if (!this) { return };
		this.setState(state);
	}

	loadNetData = () => {
		
	}

    onPushNavigator = (compent) => {
        const { navigate } = this.props.navigation;
        navigate( compent , {
            
        })
    }     

    doRegister = () => {
        let url = NetApi.register,
            mobile = this.state.mobile,
            code = this.state.code,
            password = this.state.passwd,
            relpassword = this.state.passwdd;

		if (mobile == '') {
            toastShort('手机号不能为空');
            return false;
        }
        if (!(/^1[34578]\d{9}$/.test(mobile))){
            toastShort('手机号格式不正确');
            return;
        }
        if (code == '') {
            toastShort('手机验证码不能为空');
            return false;
        }
        if (password == '' || relpassword == '') {
            toastShort('密码不能为空');
            return false;
        }else if(password !== relpassword){
        	toastShort('两次输入密码不一致');
            return false;
        }

        let data = {
            mobile: mobile,
            code: code,
            password: password,
            relpassword: relpassword,
        };
        this.netRequest.fetchPost(url, data)
            .then( result => {
            	console.log(result);
                if (result && result.code == 1) {
                    toastShort('注册成功');
                    let user = result.data;

                    this.updateState({
                        user: user
                    });

                    storage.save({
                        key: 'loginState',
                        data: {
                            id: user.id,
                            level_name: user.level_name,
                            mobile: user.mobile,
                            realname: user.realname,
                            username: user.username,
                        },
                    });

                    global.user = {
                        loginState: true,
                        userData: {
                            id: user.id,
                            level_name: user.level_name,
                            mobile: user.mobile,
                            realname: user.realname,
                            username: user.username,
                        }
                    };

                    console.log(global.user);

                    setTimeout(() => {
                        const resetAction = NavigationActions.reset({
                            index: 0,
                            actions: [
                                NavigationActions.navigate({ routeName: 'TabNavScreen'})
                            ]
                        })
                        this.props.navigation.dispatch(resetAction)
                    }, 500)
                }else{
                    toastShort(result.msg);
                }
            })
            .catch( error => {
                toastShort('服务器请求失败，请稍后重试！');
            })
    }

    getVerifyCode = () => {
        let url = NetApi.sendSMS,
            mobile = this.state.mobile;

        if (mobile == '') {
            toastShort('手机号不能为空');
            return false;
        }
        if (!(/^1[34578]\d{9}$/.test(mobile))){
            toastShort('手机号格式不正确');
            return;
        }

        let data = {
            mobile: mobile,
            type: 1
        };
        this.netRequest.fetchPost(url, data)
            .then( result => {
                if (result && result.code == 1) {
                    this.countDownTimer();
                    toastShort('验证码已发送，请注意查收！');
                }else{
                    toastShort(result.msg);
                }
            })
            .catch( error => {
                toastShort('服务器请求失败，请稍后重试！');
            })
    }

    toWebview = (title,link,compent) => {
        const { navigate } = this.props.navigation;
        navigate(compent, {
            title:title,
            link:link,
        })
    }

    // 验证码倒计时
    countDownTimer(){
        this.setState({
            codeAlreadySend: true,
            seconds: 60,
        })
        this.timerInterval = setInterval(() => {
            if (this.state.seconds === 0) {
                return clearInterval(this.timerInterval);
            }

            this.setState({
                seconds: this.state.seconds - 1
            });
        }, 1000)
    }

	render(){
		return (
			<View style={styles.container}>
                <KeyboardAwareScrollView keyboardShouldPersistTaps={'handled'}>
    				<View style={styles.logintop}>
    					<Image source={require('../../assets/images/images/images_login_bg.png')} style={styles.loginlogo} />
    					<TouchableOpacity onPress={() => {this.onBack()}} style={styles.loginback}><Image source={require('../../assets/images/icons/icon_angle_left_white.png')} style={styles.loginbackico} /></TouchableOpacity>
                        <TouchableOpacity onPress={() => {this.onBack()}} style={[styles.loginreg, GlobalStyles.hide]}>
                            <Text style={styles.loginrega}>登录</Text>
                        </TouchableOpacity>
    				</View>
    				<View style={styles.loginwrap}>
                        <View style={styles.mcell}>
                            <View style={styles.cellItem}>
                                <View style={styles.cellLeft}>
                                    <Image source={require('../../assets/images/icons/icon_mobile.png')} style={styles.leftico} />
                                </View>
                                <View style={styles.cellRight}>
                                    <TextInput placeholder="请输入手机号" keyboardType="numeric" maxLength={11} onChangeText={(text) => {
                                            this.setState({
                                                mobile:text
                                            });
                                        }}
                                        style={[styles.cellInput,GlobalStyles.isIOS ? null : styles.inputAndroid]} 
                                        underlineColorAndroid={'transparent'}
                                        placeholderTextColor="#989898"
                                    >
                                    </TextInput>
                                </View>
                            </View>
                            <View style={styles.cellItem}>
                                <View style={styles.cellLeft}>
                                    <Image source={require('../../assets/images/icons/icon_code.png')} style={styles.leftico} />
                                </View>
                                <View style={styles.cellRight}>
                                    <TextInput placeholder="请输入短信验证码" keyboardType="numeric" maxLength={6} onChangeText={(text) => {
                                            this.setState({
                                                code:text
                                            });
                                        }}
                                        style={[styles.cellInput,GlobalStyles.isIOS ? null : styles.inputAndroid]} 
                                        underlineColorAndroid={'transparent'}
                                        placeholderTextColor="#989898"
                                    >
                                    </TextInput>
                                    <View style={styles.mimaright}>
                                        {this.state.codeAlreadySend ?
                                            <View>
                                                {this.state.seconds === 0 ?
                                                    <Text style={styles.forget} onPress={()=>{this.getVerifyCode()}}>重新获取</Text>
                                                    :
                                                    <Text style={styles.forget}>剩余{this.state.seconds}秒</Text>
                                                }
                                            </View>
                                            :
                                            <Text style={styles.forget} onPress={()=>{this.getVerifyCode()}}>获取验证码</Text>
                                        }
                                    </View>
                                </View>
                            </View>
                            <View style={styles.cellItem}>
                                <View style={styles.cellLeft}>
                                    <Image source={require('../../assets/images/icons/icon_password.png')} style={styles.leftico} />
                                </View>
                                <View style={styles.cellRight}>
                                    <TextInput placeholder="请输入密码" keyboardType="default" secureTextEntry={true} maxLength={16} onChangeText={(text) => {
                                            this.setState({
                                                passwd:text
                                            });
                                        }}
                                        style={[styles.cellInput,GlobalStyles.isIOS ? null : styles.inputAndroid]} 
                                        underlineColorAndroid={'transparent'}
                                        placeholderTextColor="#989898"
                                    >
                                    </TextInput>
                                </View>
                            </View>
                            <View style={styles.cellItem}>
                                <View style={styles.cellLeft}>
                                    <Image source={require('../../assets/images/icons/icon_password.png')} style={styles.leftico} />
                                </View>
                                <View style={styles.cellRight}>
                                    <TextInput placeholder="再次输入密码" keyboardType="default" secureTextEntry={true} maxLength={16} onChangeText={(text) => {
                                            this.setState({
                                                passwdd:text
                                            });
                                        }}
                                        style={[styles.cellInput,GlobalStyles.isIOS ? null : styles.inputAndroid]} 
                                        underlineColorAndroid={'transparent'}
                                        placeholderTextColor="#989898"
                                    >
                                    </TextInput>
                                </View>
                            </View>
                        </View>
                        <View style={styles.xieyiwrap}>
                        	<Text style={styles.xieyitext}>注册后表示您同意</Text>
                        	<TouchableOpacity onPress={()=>this.toWebview('《掌上山科用户协议》',NetApi.base+NetApi.xieyi,'WebViewPage')}>
                        		<Text style={styles.xieyia}>《掌上山科用户协议》</Text>
                        	</TouchableOpacity>
                        </View>
                        <TouchableOpacity style={[styles.btn,styles.bgActive]} onPress={()=>this.doRegister()}>
                            <Text style={styles.btna}>注 册</Text>   
                        </TouchableOpacity>
                            
                    </View>
                </KeyboardAwareScrollView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
container: {
	flex: 1,
	backgroundColor: GlobalStyles.bgColor,
},
logintop:{
    width: GlobalStyles.width,
    height: GlobalStyles.width*650/750,
},
loginlogo:{
    width: GlobalStyles.width,
    height: GlobalStyles.width*650/750,
},
loginback:{
    position: 'absolute',
    top:32,
    left:8,

},
loginbackico:{
    width: 20,
    height: 20
},
loginreg:{
	position: 'absolute',
    top:35,
    right:15,
    backgroundColor:'transparent'
},
loginrega:{
    color: '#fff',
    fontSize: 15
},
loginwrap:{
    marginTop:10,
    backgroundColor:'transparent'
},mcell:{
    position:'relative',
    zIndex:1,
    marginLeft:10,
    marginRight:10,
},
cellItem:{
    overflow:'hidden',
    borderRadius:5,
    position:'relative',
    marginTop:10,
    marginBottom:0,
    paddingLeft:10,
    display:'flex',
    overflow:'hidden',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
},
cellLeft:{
    height:45,
    alignItems:'center',
    justifyContent:'center'
},
leftico:{
    width:28,
    height:28,
},
cellRight:{
    height:45,
    width: GlobalStyles.width - 70,
    alignItems:'center',
    justifyContent:'center',
    borderBottomColor:'rgba(152, 152, 152, 0.3)',
    borderBottomWidth: 1,

},
cellInput:{
    height:45,
    fontSize:15,
    textAlign:'left',
    color:'#585858',
    width: GlobalStyles.width - 70,
},
btn:{
    backgroundColor: GlobalStyles.themeColor,
    width: GlobalStyles.width*0.84,
    overflow:'hidden',
    height:45,
    borderRadius:22.5,
    marginTop:12,
    marginLeft: GlobalStyles.width*0.08,
    alignItems:'center',
    justifyContent:'center'
},
btna:{
    color:'#ffffff',
    textAlign:'center',
    fontSize:16,
},
loginbot:{
    backgroundColor:'transparent',
    marginTop:20,
    justifyContent:'space-between',
    alignItems:'center',
    height:30,
    width: GlobalStyles.width,
    flexDirection:'row',
},
botleft:{
    marginLeft:20
},
botright:{
    marginRight:20
},
bottext:{
    color:'#989898',
    fontSize:15,
    textAlign:'center'
},
botmid:{
    flex:1,
},
mimaright:{
    position:'absolute',
    right:10,
    display:'flex',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
},
forget:{
    lineHeight: GlobalStyles.isIOS ? 30 :25, 
    height:30,
    color: GlobalStyles.themeColor,
    fontSize:14,
},
xieyiwrap:{
	display:'flex',
	flexDirection: 'row',
	alignItems:'center',
	justifyContent:'center',
	marginTop: 12
},
xieyitext:{
	color: '#989898',

},
xieyia:{
	color: GlobalStyles.themeColor,
    fontSize: 12
}
});
