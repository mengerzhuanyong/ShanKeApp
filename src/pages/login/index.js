/**
 * 掌上山科APP
 * http://www.sdust.edu.cn/
 * 山科技术团队倾情打造
 */

import React, {Component} from 'react'
import {
	Text,
	View,
	Image,
	TextInput,
	ScrollView,
	StyleSheet,
	TouchableOpacity
} from 'react-native'

import { NavigationActions } from 'react-navigation'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'

export default class Login extends Component {

	constructor(props) {
		super(props);
		this.state =  {
            user: '',
            mobile:'',
            passwd:'',
            loginState: '',
        }
		this.netRequest = new NetRequest();
	}

    async componentWillMount(){
        try {
            let result = await storage.load({
                key: 'loginState',
            });
            const resetAction = NavigationActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({ routeName: 'TabNavScreen'})
                ]
            });
            this.props.navigation.dispatch(resetAction);
        } catch (error) {
            // console.log(error);
        }

    }

	componentDidMount(){
		this.loadNetData();
	}

	onBack = () => {
		this.props.navigation.goBack();
	}

	updateState= (state) => {
		if (!this) { return };
		this.setState(state);
	}

	loadNetData = () => {
		
	}

    onPushNavigator = (compent) => {
        const { navigate } = this.props.navigation;
        navigate( compent , {
            
        })
    }    

    doLogin = () => {
        let url = NetApi.login,
            mobile = this.state.mobile,
            password = this.state.passwd;

        if (mobile == '') {
            toastShort('手机号不能为空');
            return false;
        }
        if (password == '') {
            toastShort('密码不能为空');
            return false;
        }

        let data = {
            mobile: mobile,
            password: password,
        };
        this.netRequest.fetchPost(url, data)
            .then( result => {
                console.log(result.code);
                if (result && result.code == 1) {
                    console.log(result);
                    let user = result.data;

                    this.updateState({
                        user: user
                    });

                    storage.save({
                        key: 'loginState',
                        data: {
                            id: user.id,
                            level_name: user.level_name,
                            mobile: user.mobile,
                            realname: user.realname,
                            username: user.username,
                        },
                    });

                    global.user = {
                        loginState: true,
                        userData: {
                            id: user.id,
                            level_name: user.level_name,
                            mobile: user.mobile,
                            realname: user.realname,
                            username: user.username,
                        }
                    };

                    console.log(global.user);

                    // setTimeout(() => {
                        const resetAction = NavigationActions.reset({
                            index: 0,
                            actions: [
                                NavigationActions.navigate({ routeName: 'TabNavScreen'})
                            ]
                        })
                        this.props.navigation.dispatch(resetAction)
                    // }, 500)
                }else{
                    toastShort(result.msg);
                }
            })
            .catch( error => {
                toastShort('服务器请求失败，请稍后重试！');
            })
    }

	render(){
		return (
			<View style={styles.container}>
                <KeyboardAwareScrollView keyboardShouldPersistTaps={'handled'}>
    				<View style={styles.logintop}>
    					<Image source={require('../../assets/images/images/images_login_bg.png')} style={styles.loginlogo} />
    					<TouchableOpacity onPress={() => {this.onBack()}} style={[styles.loginback, {display: 'none'}]}><Image source={require('../../assets/images/icons/icon_angle_left_white.png')} style={styles.loginbackico} /></TouchableOpacity>
                        <TouchableOpacity onPress={() => {this.onPushNavigator('Home')}} style={[styles.loginreg, {display: 'none'}]}>
                            <Text style={styles.loginrega}>先不登录</Text>
                        </TouchableOpacity>
                        <View style={[styles.toptit]}>
                            <Text style={styles.toptitText}>掌上山科   因你精彩</Text>
                        </View>
    				</View>
    				<View style={styles.loginwrap}>
                        <View style={styles.mcell}>
                            <View style={styles.cellItem}>
                                <View style={styles.cellLeft}>
                                    <Image source={require('../../assets/images/icons/icon_mobile.png')} style={styles.leftico} />
                                </View>
                                <View style={styles.cellRight}>
                                    <TextInput placeholder="请输入手机号" keyboardType="numeric" maxLength={11} onChangeText={(text) => {
                                            this.setState({
                                                mobile:text
                                            });
                                        }}
                                        style={[styles.cellInput,GlobalStyles.isIOS ? null : styles.inputAndroid]} 
                                        underlineColorAndroid={'transparent'}
                                        placeholderTextColor="#989898"
                                    >
                                    </TextInput>
                                </View>
                            </View>
                            <View style={styles.cellItem}>
                                <View style={styles.cellLeft}>
                                    <Image source={require('../../assets/images/icons/icon_password.png')} style={styles.leftico} />
                                </View>
                                <View style={styles.cellRight}>
                                    <TextInput placeholder="请输入密码" keyboardType="default" secureTextEntry={true} maxLength={16} onChangeText={(text) => {
                                            this.setState({
                                                passwd:text
                                            });
                                        }}
                                        style={[styles.cellInput,GlobalStyles.isIOS ? null : styles.inputAndroid]} 
                                        underlineColorAndroid={'transparent'}
                                        placeholderTextColor="#989898"
                                    >
                                    </TextInput>
                                </View>
                            </View>
                        </View>
                        <TouchableOpacity onPress={()=>this.doLogin()}>
                            <View style={[styles.btn,styles.bgActive]}>
                                <Text style={styles.btna}>登 录</Text>   
                            </View>
                        </TouchableOpacity>
                        <View style={styles.loginbot}>
                            <TouchableOpacity onPress={() => {this.onPushNavigator('Register')}} style={styles.botleft}>
                                <Text style={styles.bottext}>注册账号</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => {this.onPushNavigator('Repassword')}} style={styles.botright}>
                                <Text style={styles.bottext}>忘记密码</Text>
                            </TouchableOpacity>
                        </View>
                            
                    </View>
                </KeyboardAwareScrollView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
container: {
	flex: 1,
	backgroundColor: GlobalStyles.bgColor,
},
logintop:{
    width: GlobalStyles.width,
    height: GlobalStyles.width*650/750,
},
loginlogo:{
    width: GlobalStyles.width,
    height: GlobalStyles.width*650/750,
},
loginback:{
    position: 'absolute',
    top:32,
    left:8,
},
toptit: {
    position: 'absolute',
    top: 35,
    backgroundColor: 'transparent',
    display: 'none',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: GlobalStyles.width,
},
toptitText: {
    fontSize: 18,
    color: '#fff',

},
loginbackico:{
    width: 20,
    height: 20
},
loginreg:{
    position: 'absolute',
    top:35,
    right:15,
    backgroundColor:'transparent'
},
loginrega:{
    color: '#fff',
    fontSize: 15
},
loginwrap:{
    marginTop:45,
    backgroundColor:'transparent'
},mcell:{
    position:'relative',
    zIndex:1,
    marginLeft:10,
    marginRight:10,
},
cellItem:{
    overflow:'hidden',
    borderRadius:5,
    position:'relative',
    marginTop:10,
    marginBottom:5,
    paddingLeft:10,
    display:'flex',
    overflow:'hidden',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
},
cellLeft:{
    height:45,
    alignItems:'center',
    justifyContent:'center'
},
leftico:{
    width:28,
    height:28,
},
cellRight:{
    height:45,
    width: GlobalStyles.width - 70,
    alignItems:'center',
    justifyContent:'center',
    borderBottomColor:'rgba(152, 152, 152, 0.3)',
    borderBottomWidth: 1,

},
cellInput:{
    height:45,
    fontSize:15,
    textAlign:'left',
    color:'#585858',
    width: GlobalStyles.width - 70,
},
btn:{
    backgroundColor: GlobalStyles.themeColor,
    width: GlobalStyles.width*0.84,
    overflow:'hidden',
    height:45,
    borderRadius:22.5,
    marginTop:20,
    marginLeft: GlobalStyles.width*0.08,
    alignItems:'center',
    justifyContent:'center'
},
btna:{
    color:'#ffffff',
    textAlign:'center',
    fontSize:16,
},
loginbot:{
    backgroundColor:'transparent',
    marginTop:20,
    justifyContent:'space-between',
    alignItems:'center',
    height:30,
    width: GlobalStyles.width,
    flexDirection:'row',
},
botleft:{
    marginLeft:20
},
botright:{
    marginRight:20
},
bottext:{
    color:'#989898',
    fontSize:15,
    textAlign:'center'
},
botmid:{
    flex:1,
},
});
