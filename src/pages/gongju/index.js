/**
 * 掌上山科APP
 * http://www.sdust.edu.cn/
 * 山科技术团队倾情打造
 */

import React, {Component} from 'react'
import {
	Text,
	View,
	Image,
	TextInput,
	ScrollView,
	StyleSheet,
	StatusBar,
	TouchableOpacity
} from 'react-native'

import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'

export default class Gongju extends Component {

	constructor(props) {
		super(props);
		this.state =  {}
		this.netRequest = new NetRequest();
	}

	componentDidMount(){
		this.loadNetData();
	}

	onBack = () => {
		this.props.navigation.goBack();
	}

	updateState= (state) => {
		if (!this) { return };
		this.setState(state);
	}

	loadNetData = () => {
		
	}

    toWebview = (title,link,compent) => {
        const { navigate } = this.props.navigation;
        navigate(compent, {
            title:title,
            link:link,
        })
    }

	render(){
		return (
			<View style={styles.container}>
				<NavigationBar
					title = {'常用导航'}
				/>
				<View style={[styles.flexColumnCenter, {marginTop: 20}]}>
					<View style={[styles.flexRowCenter, {height: 120}]}>
						<TouchableOpacity onPress={() => {this.toWebview('成绩查询', 'http://112.124.54.19/Score/index.html?identity=3A3F34E4EF6BB4F4F9450FF39C6D810B', 'WebViewPage')}} style={[styles.flexColumnCenter, {flex: 1}]}>
							<Image source={require('../../assets/images/icons/chengji.png')} style={styles.gongjuIco} />
							<Text style={styles.gongjuText}>成绩查询</Text>
						</TouchableOpacity>
						<TouchableOpacity onPress={() => {this.toWebview('火车汽车票', 'http://m.ctrip.com/webapp/train?allianceid=326093&sid=1035220&hiderecommapp=1&popup=close&autoawaken=close&showbartype=2&recommapp=0#/index', 'WebViewPage')}} style={[styles.flexColumnCenter, {flex: 1}]}>
							<Image source={require('../../assets/images/icons/chepiao.png')} style={styles.gongjuIco} />
							<Text style={styles.gongjuText}>火车汽车票</Text>
						</TouchableOpacity>
						<TouchableOpacity onPress={() => {this.toWebview('景点门票', 'https://m.ctrip.com/webapp/ticket/index?from=https%3A%2F%2Fm.ctrip.com%2Fhtml5%2F', 'WebViewPage')}} style={[styles.flexColumnCenter, {flex: 1}]}>
							<Image source={require('../../assets/images/icons/jingdian.png')} style={styles.gongjuIco} />
							<Text style={styles.gongjuText}>景点门票</Text>
						</TouchableOpacity>
					</View>
					<View style={[styles.flexRowCenter, {height: 120}]}>
						<TouchableOpacity onPress={() => {this.toWebview('校园招聘', 'https://luna.58.com/m/autotemplate?city=qd&creativeid=45&utm_source=link&spm=u-LlVFdJga1luDubj.mzp_zpdl', 'WebViewPage')}} style={[styles.flexColumnCenter, {flex: 1}]}>
							<Image source={require('../../assets/images/icons/zhaopin.png')} style={styles.gongjuIco} />
							<Text style={styles.gongjuText}>校园招聘</Text>
						</TouchableOpacity>
						<TouchableOpacity onPress={() => {this.toWebview('同城租房', 'https://luna.58.com/m/autotemplate?utm_source=link&spm=u-LlVFdJga1luDubj.mzf_zftc&creativeid=93&city=qd', 'WebViewPage')}} style={[styles.flexColumnCenter, {flex: 1}]}>
							<Image source={require('../../assets/images/icons/zufang.png')} style={styles.gongjuIco} />
							<Text style={styles.gongjuText}>同城租房</Text>
						</TouchableOpacity>
						<TouchableOpacity onPress={() => {this.toWebview('超级课堂', 'http://ke.super.cn/', 'WebViewPage')}} style={[styles.flexColumnCenter, {flex: 1}]}>
							<Image source={require('../../assets/images/icons/ketang.png')} style={styles.gongjuIco} />
							<Text style={styles.gongjuText}>超级课程</Text>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	flexRowCenter: {
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},
	flexColumnCenter: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
	},
	gongjuIco: {
		width: 55,
		height: 55,
	},
	gongjuText: {
		color: '#333',
		fontSize: 14,
		marginTop: 10
	},
});
