/**
 * 掌上山科APP
 * http://www.sdust.edu.cn/
 * 山科技术团队倾情打造
 */

import React, {Component} from 'react'
import {
	Text,
	View,
	Image,
	TextInput,
	ScrollView,
	StyleSheet,
	StatusBar,
	TouchableOpacity
} from 'react-native'

import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'

export default class ChatPage3 extends Component {

	constructor(props) {
		super(props);
		this.state =  {
			content: ''
		}
		this.netRequest = new NetRequest();
	}

	componentDidMount(){
		this.loadNetData();
	}

	onBack = () => {
		this.props.navigation.goBack();
	}

	updateState= (state) => {
		if (!this) { return };
		this.setState(state);
	}

	loadNetData = () => {
		
	}


    onPushNavigator = (compent) => {
        const { navigate } = this.props.navigation;
        navigate( compent , {
            
        })
    }

    submit = () => {
    	if(!this.state.content){
    		toastShort('内容不能为空！');
    		return false;
    	}
    	toastShort('您已成功提交，请耐心等待审核！');
    }

	render(){
        let navigationBar = {
            backgroundColor: '#fff',
            borderBottomColor: '#f2f2f2',
            borderBottomWidth: 1
        }
		return (
			<View style={styles.container}>
				<NavigationBar
					title = {'发帖'}
					style = {navigationBar}
                    titleStyle = {{color: '#333333'}}
					leftButton = {UtilsView.getLeftBlackButton(() => this.onBack())}
				/>
				<StatusBar
					animated = {true}
					hidden = {false}
					backgroundColor = {'#fff'}
					translucent = {true}
					barStyle = {'dark-content'}
				/>
                <TextInput 
                    placeholder='请输入内容~'
                    onChangeText={(text) => {
                        this.setState({content:text});
                    }}
                    style={[styles.textarea,GlobalStyles.isIOS == 'ios' ? null : styles.inputAndroid]} 
                    multiline={true}
                    underlineColorAndroid={'transparent'}
                />
              <TouchableOpacity onPress={()=>this.submit()}>
                  <View style={styles.mbutton}>
                      <Text style={styles.mbuttona}>提 交</Text>
                  </View>
              </TouchableOpacity>
				
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: GlobalStyles.bgColor,
	},
	textarea:{
	    width: GlobalStyles.width,
	    height:150,
	    backgroundColor:'#fff',
	    lineHeight:36,
	    padding:15,
	    textAlignVertical:'top',
	},
	mbutton:{
	    width: GlobalStyles.width*0.9,
	    marginLeft: GlobalStyles.width*0.05,
	    borderRadius:5,
	    height:45,
	    overflow:'hidden',
	    marginTop:20,
	    marginBottom:20,
	    backgroundColor: GlobalStyles.themeColor,
	    justifyContent:'center',
	    alignItems:'center'
	},
	mbuttona:{
	    color:'#fff',
	    width: GlobalStyles.width*0.9,
	    textAlign:'center',
	    fontSize:18,
	},
});
