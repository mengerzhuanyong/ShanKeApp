/**
 * 掌上山科APP
 * http://www.sdust.edu.cn/
 * 山科技术团队倾情打造
 */

import React, {Component} from 'react'
import {
	Text,
	View,
	Image,
	TextInput,
	ScrollView,
	StyleSheet,
	StatusBar,
	TouchableOpacity
} from 'react-native'

import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'

export default class Chat extends Component {

	constructor(props) {
		super(props);
		this.state =  {}
		this.netRequest = new NetRequest();
	}

	componentDidMount(){
		this.loadNetData();
	}

	onBack = () => {
		this.props.navigation.goBack();
	}

	updateState= (state) => {
		if (!this) { return };
		this.setState(state);
	}

	loadNetData = () => {
		
	}


    onPushNavigator = (compent) => {
        const { navigate } = this.props.navigation;
        navigate( compent , {
            
        })
    }

	render(){
		return (
			<View style={styles.container}>
				<NavigationBar
					title = {'下课聊'}
				/>
				<ScrollView>
					<TouchableOpacity onPress={() => {this.onPushNavigator('ChatPage1')}} style={styles.tiebalist}>
				        <View style={styles.tbtop}>
				            <Image source={{uri: 'http://e.fxshe.com/Uploads/confession/20180110/20180110100616.jpeg'}} style={styles.touxiang} />
				            <View style={styles.tbtright}>
				                <Text style={styles.tbauthor}>韩倩</Text>
				                <View style={[styles.tbtbot, GlobalStyles.flexRowStart]}>
					                <Text style={styles.tbtime}>04-16 08:30</Text>
					                <Text style={styles.tbfrom}>来自[艺术与设计学院]</Text>
				                </View>
				            </View>
				        </View>
				        <View style={styles.tbmid}>
				            <Text style={styles.tbtext}>#单身狗的日常#  （爆照）然后大家都来说说自己想找个什么类型的对象，觉得合适的就在一起吧~</Text>
				            <View style={styles.tbpic}>
				                <Image source={{uri: 'http://e.fxshe.com/Uploads/confession/20180110/20180110100700.jpeg'}} style={styles.liaopic} />
				            </View>
				        </View>             
				    </TouchableOpacity>

				    <TouchableOpacity onPress={() => {this.onPushNavigator('ChatPage2')}} style={styles.tiebalist}>
				        <View style={styles.tbtop}>
				            <Image source={{uri: 'http://e.fxshe.com/Uploads/confession/20180110/20180110100535.jpeg'}} style={styles.touxiang} />
				            <View style={styles.tbtright}>
				                <Text style={styles.tbauthor}>金晨</Text>
				                <View style={[styles.tbtbot, GlobalStyles.flexRowStart]}>
					                <Text style={styles.tbtime}>04-10 19:36</Text>
					                <Text style={styles.tbfrom}>来自[计算机学院]</Text>
				                </View>
				            </View>
				        </View>
				        <View style={styles.tbmid}>
				            <Text style={styles.tbtext}>#单身狗的日常# “学校旁的广场，我在这等钟声响，等你一起下课好吗？”</Text>
				            <View style={styles.tbpic}>
				                <Image source={{uri: 'http://e.fxshe.com/Uploads/confession/20180110/20180110100700.jpeg'}} style={styles.liaopic} />
				            </View>
				        </View>             
				    </TouchableOpacity>

				    <TouchableOpacity onPress={() => {this.onPushNavigator('ChatPage3')}} style={styles.tiebalist}>
				        <View style={styles.tbtop}>
				            <Image source={{uri: 'http://e.fxshe.com/Uploads/confession/20180106/20180106213955.jpeg'}} style={styles.touxiang} />
				            <View style={styles.tbtright}>
				                <Text style={styles.tbauthor}>田子涵</Text>
				                <View style={[styles.tbtbot, GlobalStyles.flexRowStart]}>
					                <Text style={styles.tbtime}>03-16 18:47</Text>
					                <Text style={styles.tbfrom}>来自[海天一色论坛]</Text>
				                </View>
				            </View>
				        </View>
				        <View style={styles.tbmid}>
				            <Text style={styles.tbtext}>#我这么美我不能死# 跳珠渐雪毕玲珑</Text>
				            <View style={styles.tbpic}>
				                <Image source={{uri: 'http://e.fxshe.com/Uploads/confession/20180110/20180110100700.jpeg'}} style={styles.liaopic} />
				            </View>
				        </View>             
				    </TouchableOpacity>

				    <TouchableOpacity onPress={() => {this.onPushNavigator('ChatPage4')}} style={styles.tiebalist}>
				        <View style={styles.tbtop}>
				            <Image source={{uri: 'http://e.fxshe.com/Uploads/confession/20180110/20180110100433.jpeg'}} style={styles.touxiang} />
				            <View style={styles.tbtright}>
				                <Text style={styles.tbauthor}>刘艺璇</Text>
				                <View style={[styles.tbtbot, GlobalStyles.flexRowStart]}>
					                <Text style={styles.tbtime}>01-27 11:32</Text>
					                <Text style={styles.tbfrom}>来自[神秘组织]</Text>
				                </View>
				            </View>
				        </View>
				        <View style={styles.tbmid}>
				            <Text style={styles.tbtext}>#单身狗的日常#  现在大二，一个人存钱，一个人去青海，一个人去西安，一个人去甘肃重庆大理河北郑州，一个人路过半个中国，就算没遇到你，也要记得出发！晚安，陌生人！</Text>
				            <View style={styles.tbpic}>
				                <Image source={{uri: 'http://e.fxshe.com/Uploads/confession/20180110/20180110100700.jpeg'}} style={styles.liaopic} />
				            </View>
				        </View>             
				    </TouchableOpacity>

				    <TouchableOpacity onPress={() => {this.onPushNavigator('ChatPage5')}} style={styles.tiebalist}>
				        <View style={styles.tbtop}>
				            <Image source={{uri: 'http://e.fxshe.com/Uploads/confession/20180110/20180110100616.jpeg'}} style={styles.touxiang} />
				            <View style={styles.tbtright}>
				                <Text style={styles.tbauthor}>王琳</Text>
				                <View style={[styles.tbtbot, GlobalStyles.flexRowStart]}>
					                <Text style={styles.tbtime}>03-10 03:01</Text>
					                <Text style={styles.tbfrom}>来自[矿业学院]</Text>
				                </View>
				            </View>
				        </View>
				        <View style={styles.tbmid}>
				            <Text style={styles.tbtext}>#我这么美我不能死#  第一次拍胶片......一首凉凉送给自己和摄影师......</Text>
				            <View style={styles.tbpic}>
				                <Image source={{uri: 'http://e.fxshe.com/Uploads/confession/20180110/20180110100700.jpeg'}} style={styles.liaopic} />
				            </View>
				        </View>             
				    </TouchableOpacity>
			    </ScrollView>	

			    <TouchableOpacity onPress={() => {this.onPushNavigator('Fatie')}} style={styles.fatiebtn}>
					<Image source={require('../../assets/images/icons/fatie.png')} style={styles.fatiebtna} />
			    </TouchableOpacity>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: GlobalStyles.bgColor,
	},
	tiebalist: {
		backgroundColor: '#fff',
	    padding: 15,
	    marginBottom: 10,
	    paddingBottom: 0,
	},
	tbtop: {
		height: 45,
	    display: 'flex',
	    flexDirection: 'row',
	    justifyContent: 'flex-start',
	    alignItems: 'center',
	},
	touxiang: {
		width: 44,
	    height: 44,
	    borderRadius: 22,
	    marginRight: 10,
	},
	tbtright: {
		flex: 1,
	},
	tbauthor: {
		color: '#333333',
	    fontSize: 15,
	    lineHeight: 25,
	},
	tbtbot: {
	    position: 'relative',
	    marginTop: 3
	},
	tbtime: {
		marginRight: 10,
		color: '#999999',
	    fontSize: 11,
	    height: 18,
	},
	tbfrom: {
		color: '#999999',
	    fontSize: 11,
	    height: 18,
	},
	tbmid: {
		marginTop: 10
	},
	tbtext: {
	    marginTop: 5,
	    color: '#333',
	    lineHeight: 20,
	    fontSize: 15,
	},
	tbpic: {
	    width: GlobalStyles.width,
	    marginTop: 10,
	},
	liaopic: {

	},
	fatiebtn: {
	    width: 54,
	    height: 54,
	    backgroundColor: '#fff',
	    borderRadius: 27,
	    position: 'absolute',
	    right: 20,
	    bottom: 20
	},
	fatiebtna: {
		width: 54,
		height: 54,
	},
});
