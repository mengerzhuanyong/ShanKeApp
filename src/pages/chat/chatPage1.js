/**
 * 掌上山科APP
 * http://www.sdust.edu.cn/
 * 山科技术团队倾情打造
 */

import React, {Component} from 'react'
import {
	Text,
	View,
	Image,
	TextInput,
	ScrollView,
	StyleSheet,
	StatusBar,
	TouchableOpacity
} from 'react-native'

import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'

export default class ChatPage1 extends Component {

	constructor(props) {
		super(props);
		this.state =  {}
		this.netRequest = new NetRequest();
	}

	componentDidMount(){
		this.loadNetData();
	}

	onBack = () => {
		this.props.navigation.goBack();
	}

	updateState= (state) => {
		if (!this) { return };
		this.setState(state);
	}

	loadNetData = () => {
		
	}


    onPushNavigator = (compent) => {
        const { navigate } = this.props.navigation;
        navigate( compent , {
            
        })
    }

	render(){
		let navigationBar = {
			backgroundColor: '#fff',
			borderBottomColor: '#f2f2f2',
			borderBottomWidth: 1,
		};
		return (
			<View style={styles.container}>
				<NavigationBar
					title = {'下课聊'}
                    titleStyle = {{color: '#333333'}}
					style = {navigationBar}
					leftButton = {UtilsView.getLeftBlackButton(() => this.onBack())}
				/>
				<StatusBar
					animated = {true}
					hidden = {false}
					backgroundColor = {'#fff'}
					translucent = {true}
					barStyle = {'dark-content'}
				/>
				<ScrollView>
					<View style={styles.tiebalist}>
				        <View style={styles.tbtop}>
				            <Image source={{uri: 'http://e.fxshe.com/Uploads/confession/20180110/20180110100616.jpeg'}} style={styles.touxiang} />
				            <View style={styles.tbtright}>
				                <Text style={styles.tbauthor}>韩倩</Text>
				                <View style={[styles.tbtbot, GlobalStyles.flexRowStart]}>
					                <Text style={styles.tbtime}>04-16 08:30</Text>
					                <Text style={styles.tbfrom}>来自[艺术与设计学院]</Text>
				                </View>
				            </View>
				        </View>
				        <View style={styles.tbmid}>
				            <Text style={styles.tbtext}>#单身狗的日常#  （爆照）然后大家都来说说自己想找个什么类型的对象，觉得合适的就在一起吧~</Text>
				            <View style={styles.tbpic}>
				                <Image source={require('../../assets/images/images/liao11.jpg')} style={[styles.liaopic, {height: (GlobalStyles.width-30)*1067/800}]} />
				                <Image source={require('../../assets/images/images/liao12.jpg')} style={[styles.liaopic, {height: (GlobalStyles.width-30)*1075/800}]} />
				                <Image source={require('../../assets/images/images/liao12.jpg')} style={[styles.liaopic, {height: (GlobalStyles.width-30)*1067/800}]} />
				            </View>
				        </View>                   
				    </View>
			    </ScrollView>	
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: GlobalStyles.bgColor,
	},
	tiebalist: {
		backgroundColor: '#fff',
	    padding: 15,
	    marginBottom: 20,
	    paddingBottom: 0,
	},
	tbtop: {
		height: 45,
	    display: 'flex',
	    flexDirection: 'row',
	    justifyContent: 'flex-start',
	    alignItems: 'center',
	},
	touxiang: {
		width: 44,
	    height: 44,
	    borderRadius: 22,
	    marginRight: 10,
	},
	tbtright: {
		flex: 1,
	},
	tbauthor: {
		color: '#333333',
	    fontSize: 15,
	    lineHeight: 25,
	},
	tbtbot: {
	    position: 'relative',
	    marginTop: 3
	},
	tbtime: {
		marginRight: 10,
		color: '#999999',
	    fontSize: 11,
	    height: 18,
	},
	tbfrom: {
		color: '#999999',
	    fontSize: 11,
	    height: 18,
	},
	tbmid: {
		marginTop: 10
	},
	tbtext: {
	    marginTop: 5,
	    color: '#333',
	    lineHeight: 20,
	    fontSize: 15,
	},
	tbpic: {
	    width: GlobalStyles.width - 30,
	    marginTop: 10,
	},
	liaopic: {
		width: GlobalStyles.width - 30,
		marginBottom: 10
	},
});
