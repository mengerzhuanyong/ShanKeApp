/**
 * 掌上山科APP
 * http://www.sdust.edu.cn/
 * 山科技术团队倾情打造
 */

import React, {Component} from 'react'
import {
	Text,
	View,
	Image,
	TextInput,
	ScrollView,
	StyleSheet,
	StatusBar,
	TouchableOpacity
} from 'react-native'

import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'
import { Constants } from '../../constants/Constants'

import Theme from '../../components/common/Theme'

export default class User extends Component {

	constructor(props) {
		super(props);
		this.state =  {
            user: '',
            loginState: '',
		}
		this.netRequest = new NetRequest();
	}

	componentDidMount(){
        this.loadNetData();
	}

	updateState= (state) => {
		if (!this) { return };
		this.setState(state);
	}

	loadNetData = () => {
		if (!global.user.loginState) {
            return;
        }else{
            let { userData, loginState } = global.user;
            this.updateState({
                user: userData,
                loginState: loginState,
            })
            this.getNetData(userData);
        }
	}

    getNetData = (user) => {
        console.log('本地用户信息', user);
        let url = NetApi.user + '/id/' + user.id;
        this.netRequest.fetchGet(url, true)
            .then( result => {
            	console.log('更新本地信息', result.data);
                this.updateState({
                    user: result.data,
                })
            })
            .catch( error => {
                console.log('获取信息失败', error);
            })
    }

    onPushNavigator = (compent) => {
        const { navigate } = this.props.navigation;
		navigate( compent , {
			onCallBack:()=>{
                this.loadNetData();
            }
		})
    }

	toWebview = (title,link,compent) => {
		const { navigate } = this.props.navigation;
		navigate(compent, {
			title:title,
			link:link,
		})
	}

	onBack = () => {
		if(this.props.navigation.goBack()){
			this.props.navigation.goBack();
		}else{
			// this.props.navigation.navigate('TabNavScreen');
			this.onPushNavigator('Home');
		}
	}

	render(){
		return (
			<View style={styles.container}>
				<StatusBar
					animated = {true}
					hidden = {false}
					backgroundColor = {'transparent'}
					translucent = {true}
					barStyle = {'light-content'}
				/>
				<View style={styles.usertop}>
					<Image source={require('../../assets/images/images/images_user_banner.png')} style={styles.userbg} />
					<View style={styles.userbgbot}>
						<Image source={require('../../assets/images/icons/icon_user_touxiang.png')} style={styles.userlogo} />
						<View style={styles.userlogobot}>
							<Text style={styles.userlogobottext}>{this.state.user.username ? this.state.user.username : '用户: '+this.state.user.mobile}</Text>
						</View>
					</View>
					<TouchableOpacity onPress={() => {this.onBack()}} style={[styles.userback, GlobalStyles.hide]}>
						<Image source={require('../../assets/images/icons/icon_close.png')} style={styles.userbackico} />
					</TouchableOpacity>
					<TouchableOpacity onPress={() => {this.onPushNavigator('Shezhi')}} style={styles.usershezhi}>
						<Image source={require('../../assets/images/icons/icon_shezhi.png')} style={styles.usershezhiico} />
					</TouchableOpacity>
				</View>
				<View style={GlobalStyles.usermid}>
					<TouchableOpacity onPress={() => {this.onPushNavigator('Ziliao')}} style={GlobalStyles.userlist}>
						<View style={GlobalStyles.userlistleft}>
							<Image source={require('../../assets/images/icons/icon_user_ziliao.png')} style={GlobalStyles.usericon} />
						</View>
						<View style={GlobalStyles.userlistright}>
							<Text style={GlobalStyles.userlisttext}>我的资料</Text>
							<Image source={require('../../assets/images/icons/icon_user_more.png')} style={GlobalStyles.userlistmore} />
						</View>
					</TouchableOpacity>
					<TouchableOpacity onPress={() => {this.onPushNavigator('Shoucang')}} style={GlobalStyles.userlist}>
						<View style={GlobalStyles.userlistleft}>
							<Image source={require('../../assets/images/icons/icon_user_shoucang.png')} style={GlobalStyles.usericon} />
						</View>
						<View style={GlobalStyles.userlistright}>
							<Text style={GlobalStyles.userlisttext}>我的收藏</Text>
							<Image source={require('../../assets/images/icons/icon_user_more.png')} style={GlobalStyles.userlistmore} />
						</View>
					</TouchableOpacity>
					<TouchableOpacity onPress={() => {this.toWebview('关于我们',this.state.user.coop_url,'WebViewPage')}} style={GlobalStyles.userlist}>
						<View style={GlobalStyles.userlistleft}>
							<Image source={require('../../assets/images/icons/icon_user_about.png')} style={GlobalStyles.usericon} />
						</View>
						<View style={GlobalStyles.userlistright}>
							<Text style={GlobalStyles.userlisttext}>关于我们</Text>
							<Image source={require('../../assets/images/icons/icon_user_more.png')} style={GlobalStyles.userlistmore} />
						</View>
					</TouchableOpacity>
					<View style={GlobalStyles.userlist}>
						<View style={GlobalStyles.userlistleft}>
							<Image source={require('../../assets/images/icons/icon_user_banben.png')} style={GlobalStyles.usericon} />
						</View>
						<View style={GlobalStyles.userlistright}>
							<Text style={GlobalStyles.userlisttext}>当前版本：V{Constants.VERSION_NAME}</Text>
						</View>
					</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	usertop:{
		position: 'relative',
		width: GlobalStyles.width,
		height: GlobalStyles.width*400/750,
	},
	userbg:{
		position: 'absolute',
		top: 0,
		left: 0,
		width: GlobalStyles.width,
		height: GlobalStyles.width*400/750,

	},
	userbgbot:{
		position: 'absolute',
		top:GlobalStyles.width*400/750/2,
		left:GlobalStyles.width/2,
		marginTop:-35,
		marginLeft:-100,
		height:120,
		width:200,
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center'
	},
	userlogo:{
		width: 90,
		height: 90,
		// marginBottom:5
	},
	userlogobot:{
		height:30,
		alignItems: 'center',
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'center',

	},
	userlogobottext:{
		color: '#fff',
		fontSize:16,
		backgroundColor:'transparent',
	},
	userback:{
		position:'absolute',
		top:35,
		left:15,
		// display: 'none',
	},
	userbackico:{
		width:18,
		height:18,
	},
	usershezhi:{
		position: 'absolute',
		top: Theme.statusBarHeight + 10,
		right: 15
	},
	usershezhiico: {
		width:22,
		height:22,
	},

});
