/**
 * 掌上山科APP
 * http://www.sdust.edu.cn/
 * 山科技术团队倾情打造
 */

import React, {Component} from 'react'
import {
	Text,
	View,
	Image,
	TextInput,
	ScrollView,
	StyleSheet,
	TouchableOpacity
} from 'react-native'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'

import ModalDropdown from 'react-native-modal-dropdown'

export default class Ziliao extends Component {

	constructor(props) {
		super(props);
		this.state =  {
            user: '',
            loginState: '',
            level_id: '',
            relation: [],
        }
		this.netRequest = new NetRequest();
	}

	componentDidMount(){
        this.getUserData();
		this.loadNetData();
	}

	onBack = () => {
        this.props.navigation.state.params.onCallBack();
		this.props.navigation.goBack();
	}

	updateState= (state) => {
		if (!this) { return };
		this.setState(state);
	}

	getUserData = () => {
        console.log(global.user);
        if (!global.user.loginState) {
            return;
        }else{
            let { userData, loginState } = global.user;
            this.updateState({
                user: userData,
                loginState: loginState,
            })
        }		
	}

    loadNetData = () => {
        let url = NetApi.relation;
        
        this.netRequest.fetchGet(url, true)
            .then( result => {
                console.log(result);
                if (result && result.code == 1) {
                    this.updateState({
                        relation: result.data
                    })
                }
            })
            .catch( error => {
                console.log(error)
            })
    }

    // 提交资料
    submitUserInfo = () => {
        let { id, realname, username, level_name } = this.state.user;
        let { level_id } = this.state;

        if (!realname) {
            toastShort('请填写真实姓名');
            return;
        }
        if (!username) {
            toastShort('请填写常用昵称');
            return;
        }
        if (!level_name) {
            toastShort('请选择与学校关系');
            return;
        }

        let url = NetApi.edituser;
        let data = {
            id: id,
            realname: realname,
            username: username,
            level: level_id,
        }
        
        this.netRequest.fetchPost(url, data, true)
            .then( result => {
                console.log(result);
                if (result && result.code == 1) {
                    toastShort('保存成功');
                    this.saveLocalStorage(result.data);
                    this.onBack();
                }
            })
            .catch( error => {
                console.log(error)
            })
    }

    saveLocalStorage = (data) => {
        console.log(data);
        storage.save({
            key: 'loginState',
            data: {
                id: data.id,
                level_name: data.level_name,
                mobile: data.mobile,
                realname: data.realname,
                username: data.username,
            },
        });

        global.user = {
            loginState: true,
            userData: data,
        };
    };


    // onSelect(index, value) {
    //     console.log(this.state.user, value);
    //     this.state.user.level_name = value;
    //     this.updateState({
    //         user: this.state.user
    //     });
    // }

    renderRow = (rowData) => {
        return (
            <View style = {GlobalStyles.dropdownRow}>
                <Text style = {GlobalStyles.dropdownRowText}>{rowData.name}</Text>
            </View>
        );
    }

    renderButtonText = (rowData) => {
        // console.log(rowData);
        const {id, name} = rowData;
        this.state.user.level_name = name;
        this.updateState({
            level_id: id,
            user: this.state.user,
        })
        console.log(name);
        return name;
    }

	render(){
		return (
			<View style={styles.container}>
				<NavigationBar
					title = {'我的资料'}
					leftButton = {UtilsView.getLeftButton(() => this.onBack())}
				/>
	            <KeyboardAwareScrollView keyboardShouldPersistTaps={'handled'} style={styles.scrollView}>
	                <View style={GlobalStyles.mcell}>
	                    <View style={GlobalStyles.cellItem}>
	                        <Text style={GlobalStyles.cellLeft}>真实姓名</Text>
	                        <View style={[GlobalStyles.cellRight,GlobalStyles.textRight]}>
	                            <TextInput
                                    placeholder={this.state.user.realname ? this.state.user.realname : '请填写真实姓名' }
                                    onChangeText={(text) => {
                                        this.state.user.realname = text;
                                        console.log(this.state.user);
                                        this.updateState({
                                            user: this.state.user
                                        })
                                    }}
                                    style={[GlobalStyles.cellInput,GlobalStyles.isIOS ? null : GlobalStyles.inputAndroid]} 
                                    underlineColorAndroid={'transparent'}
                                >
                                </TextInput>
	                        </View>
	                    </View>
	                    <View style={GlobalStyles.cellItem}>
	                        <Text style={GlobalStyles.cellLeft}>常用昵称</Text>
	                        <View style={[GlobalStyles.cellRight,GlobalStyles.textRight]}>
	                            <TextInput
                                    placeholder={this.state.user.username ? this.state.user.username : '请填写常用昵称' }
                                    onChangeText={(text) => {
                                        this.state.user.username = text;
                                        this.updateState({
                                            user: this.state.user
                                        }, () =>{
                                            console.log(this.state.user);
                                        })
                                    }}
                                    style={[GlobalStyles.cellInput,GlobalStyles.isIOS ? null : GlobalStyles.inputAndroid]} 
                                    underlineColorAndroid={'transparent'}
                                >
                                </TextInput>
	                        </View>
	                    </View>
	                    <View style={GlobalStyles.cellItem}>
	                        <Text style={GlobalStyles.cellLeft}>手机号</Text>
	                        <View style={[GlobalStyles.cellRight,GlobalStyles.textRight]}>
	                            <Text style={GlobalStyles.cellRightText}>{this.state.user.mobile}</Text>
	                        </View>
	                    </View>
	                    <View style={GlobalStyles.cellItem}>
	                        <Text style={GlobalStyles.cellLeft}>与学校关系</Text>
	                        <View style={[GlobalStyles.cellRight,GlobalStyles.textRight]}>
	                            <ModalDropdown
                                    style = {GlobalStyles.selectView}
                                    textStyle = {GlobalStyles.textStyle}
                                    dropdownStyle = {GlobalStyles.dropdownStyle}
                                    defaultValue = {this.state.user.level_name ? this.state.user.level_name : '请选择'} 
                                    renderRow={this.renderRow.bind(this)}
                                    options = {this.state.relation}
                                    renderButtonText = {(rowData) => this.renderButtonText(rowData)}
                                >
                                    <View style={GlobalStyles.selectViewWrap}>
                                        <View style={GlobalStyles.paymentMethodTitleView}>
                                            <Text style={GlobalStyles.cargoAttributesTitle}>{this.state.user.level_name ? this.state.user.level_name : '请选择'}</Text>
                                        </View>
                                    </View>
                                </ModalDropdown>
	                        </View>
	                    </View>
	                </View>  
                    <TouchableOpacity onPress={()=>this.submitUserInfo()}>
                        <View style={[GlobalStyles.btn,GlobalStyles.bgActive]}>
                            <Text style={GlobalStyles.btna}>提交</Text>   
                        </View>
                    </TouchableOpacity>
	            </KeyboardAwareScrollView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: GlobalStyles.bgColor,
	},
	scrollView:{
	    
	},
});
