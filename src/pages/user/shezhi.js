/**
 * 掌上山科APP
 * http://www.sdust.edu.cn/
 * 山科技术团队倾情打造
 */

import React, {Component} from 'react'
import {
	Text,
	View,
	Image,
	TextInput,
	ScrollView,
	StyleSheet,
	TouchableOpacity
} from 'react-native'

import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'

export default class Shezhi extends Component {

	constructor(props) {
		super(props);
		this.state =  {}
		this.netRequest = new NetRequest();
	}

	componentDidMount(){
		this.loadNetData();
	}

	onBack = () => {
		this.props.navigation.goBack();
	}

	updateState= (state) => {
		if (!this) { return };
		this.setState(state);
	}

	loadNetData = () => {
		
	}

    onPushNavigator = (compent) => {
        const { navigate } = this.props.navigation;
		navigate( compent , {
			
		})
    }

    doLogOut = () => {
        this.removeLoginState();
    }

    removeLoginState = () => {
        storage.remove({
            key: 'loginState',
        });
        global.user.loginState = false;
        global.user.userData = '';
        setTimeout(() => {
            this.props.navigation.navigate('Login'); //跳转到用户页面  
        }, 500);
    }

	// clearCache = () => {

	// }

	render(){
		return (
			<View style={styles.container}>
				<NavigationBar
					title = {'设置'}
					leftButton = {UtilsView.getLeftButton(() => this.onBack())}
				/>
				<View style={[GlobalStyles.usermid, {borderTopWidth: 10, borderTopColor: GlobalStyles.bgColor}]}>
					<TouchableOpacity onPress={() => {this.onPushNavigator('SetMobile')}} style={GlobalStyles.userlist}>
						<View style={GlobalStyles.userlistleft}>
							<Image source={require('../../assets/images/icons/icon_setmobile.png')} style={GlobalStyles.usericon} />
						</View>
						<View style={GlobalStyles.userlistright}>
							<Text style={GlobalStyles.userlisttext}>修改手机号</Text>
							<Image source={require('../../assets/images/icons/icon_user_more.png')} style={GlobalStyles.userlistmore} />
						</View>
					</TouchableOpacity>
					<TouchableOpacity onPress={() => {this.onPushNavigator('SetPassword')}} style={GlobalStyles.userlist}>
						<View style={GlobalStyles.userlistleft}>
							<Image source={require('../../assets/images/icons/icon_setpassword.png')} style={GlobalStyles.usericon} />
						</View>
						<View style={GlobalStyles.userlistright}>
							<Text style={GlobalStyles.userlisttext}>修改密码</Text>
							<Image source={require('../../assets/images/icons/icon_user_more.png')} style={GlobalStyles.userlistmore} />
						</View>
					</TouchableOpacity>
					<TouchableOpacity onPress={() => {this.clearCache()}} style={[GlobalStyles.userlist, {display: 'none'}]}>
						<View style={GlobalStyles.userlistleft}>
							<Image source={require('../../assets/images/icons/icon_cache.png')} style={GlobalStyles.usericon} />
						</View>
						<View style={GlobalStyles.userlistright}>
							<Text style={GlobalStyles.userlisttext}>清除缓存</Text>
							<Image source={require('../../assets/images/icons/icon_user_more.png')} style={GlobalStyles.userlistmore} />
						</View>
					</TouchableOpacity>
				</View>
				<TouchableOpacity onPress = {() => {this.doLogOut()}} style={styles.userbot}>
					<Text style={styles.userbottext}>退出登录</Text>
				</TouchableOpacity>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	userbot:{
		position: 'absolute',
		bottom:50,
		left:0,
		width:GlobalStyles.width,
		borderBottomWidth: 1,
		borderBottomColor: '#f2f2f2',
		borderTopWidth: 1,
		borderTopColor: '#f2f2f2',
		height: 48,
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},
	userbottext:{
		color:GlobalStyles.themeColor,
		fontSize:18,
		textAlign:'center',
	}
});
