/**
 * 掌上山科APP
 * http://www.sdust.edu.cn/
 * 山科技术团队倾情打造
 */

import React, {Component} from 'react'
import {
	Text,
	View,
	Image,
	TextInput,
	ScrollView,
	StyleSheet,
	TouchableOpacity
} from 'react-native'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'

export default class SetPassword extends Component {

	constructor(props) {
		super(props);
		this.state =  {
			user: '',
			password: '',
			rePassword: '',
		}
		this.netRequest = new NetRequest();
	}

	componentDidMount(){
        this.getUserData();
		this.loadNetData();
	}

	onBack = () => {
		this.props.navigation.goBack();
	}

	updateState= (state) => {
		if (!this) { return };
		this.setState(state);
	}

	getUserData = () => {
        console.log(global.user);
        if (!global.user.loginState) {
            return;
        }else{
            let { userData, loginState } = global.user;
            this.updateState({
                user: userData,
                loginState: loginState,
            })
        }		
	}

	loadNetData = () => {
		
	}

	submit = () => {
		let { id } = this.state.user;
		let { password, rePassword } = this.state;

		if (!password) {
            toastShort('请输入新密码');
            return;
        }
        if (!rePassword) {
            toastShort('请确认新密码');
            return;
        }
        if (password !== rePassword) {
			toastShort('两次密码输入不一致，请重新输入！');
            return;
        }

        let url = NetApi.setPassword;
        let data = {
            uid: id,
            password: password,
        }
        
        this.netRequest.fetchPost(url, data, true)
            .then( result => {
                console.log(result);
                if (result && result.code == 1) {
                    // toastShort('修改密码成功，请重新登录！');
                    this.doLogOut();
                }
            })
            .catch( error => {
                console.log(error)
            })
	}

    doLogOut = () => {
        let url = NetApi.logout;
        this.netRequest.fetchGet(url)
            .then( result => {
                console.log(result);
                this.removeLoginState();
                if (result && result.code == 0) {
                    toastShort("修改密码成功，请重新登录！");
                }
            })
            .catch( error => {
                console.log('退出失败，请重试！', error);
            })

    }

    removeLoginState = () => {
        storage.remove({
            key: 'loginState',
        });
        global.user.loginState = false;
        global.user.userData = '';
        setTimeout(() => {
            this.props.navigation.navigate('Login'); //跳转到用户页面  
        }, 500);
    }

	render(){
		return (
			<View style={styles.container}>
				<NavigationBar
					title = {'修改密码'}
					leftButton = {UtilsView.getLeftButton(() => this.onBack())}
				/>
	            <KeyboardAwareScrollView keyboardShouldPersistTaps={'handled'}>
	                <View style={[GlobalStyles.mcell, {marginTop: 0, borderTopWidth: 10, borderTopColor: GlobalStyles.bgColor}]}>
	                    <View style={GlobalStyles.cellItem}>
	                        <Text style={GlobalStyles.cellLeft}>新密码</Text>
	                        <View style={[GlobalStyles.cellRight,GlobalStyles.textRight]}>
	                            <TextInput
                                    placeholder={'请输入新密码'}
                                    secureTextEntry={true}
                                    onChangeText={(text) => {
                                        this.setState({
                                            password: text
                                        })
                                    }}
                                    style={[GlobalStyles.cellInput,GlobalStyles.isIOS ? null : GlobalStyles.inputAndroid]} 
                                    underlineColorAndroid={'transparent'}
                                >
                                </TextInput>
	                        </View>
	                    </View>
	                    <View style={GlobalStyles.cellItem}>
	                        <Text style={[GlobalStyles.cellLeft, {width: 100}]}>确认新密码</Text>
	                        <View style={[GlobalStyles.cellRight,GlobalStyles.textRight]}>
	                            <TextInput
                                    placeholder={'请确认新密码'}
                                    secureTextEntry={true}
                                    onChangeText={(text) => {
                                        this.setState({
                                            rePassword: text
                                        })
                                    }}
                                    style={[GlobalStyles.cellInput,GlobalStyles.isIOS ? null : GlobalStyles.inputAndroid]} 
                                    underlineColorAndroid={'transparent'}
                                >
                                </TextInput>
	                        </View>
	                    </View>
                    </View>
                    <TouchableOpacity style={[GlobalStyles.btn,GlobalStyles.bgActive, {marginTop: 15}]} onPress={()=>this.submit()}>
                        <Text style={GlobalStyles.btna}>提交修改</Text>   
                    </TouchableOpacity>
                </KeyboardAwareScrollView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	scrollView: {

	}
});
