/**
 * 掌上山科APP
 * http://www.sdust.edu.cn/
 * 山科技术团队倾情打造
 */

import React, {Component} from 'react'
import {
	Text,
	View,
	Image,
	TextInput,
	ScrollView,
	StyleSheet,
	TouchableOpacity
} from 'react-native'

// import {SegmentedView, Label} from 'teaset';
import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'

import MyNewsTabItemView from './myNewsTabItemView'
import MyHighlightsTabItemView from './myHighlightsTabItemView'
import MyVideoTabItemView from './myVideoTabItemView'
import MyMovieTabItemView from './myMovieTabItemView'
import MyPicTabItemView from './myPicTabItemView'
import MyNewspaperTabItemView from './myNewspaperTabItemView'
import MyNoticeTabItemView from './myNoticeTabItemView'
import SegmentedView from '../../components/segmented/SegmentedView'

// import ScrollableTabView, {ScrollableTabBar, DefaultTabBar} from 'react-native-scrollable-tab-view';
// import DefaultTabBar from '../../components/common/defaultTabBar'

export default class Shoucang extends Component {

	constructor(props) {
		super(props);
		this.state =  {}
		this.netRequest = new NetRequest();
	}

	componentDidMount(){
		this.loadNetData();
	}

	onBack = () => {
		this.props.navigation.goBack();
	}

	updateState= (state) => {
		if (!this) { return };
		this.setState(state);
	}

	loadNetData = () => {
		
	}

	render(){
		return (
			<View style={styles.container}>
				<NavigationBar
					title = {'我的收藏'}
					leftButton = {UtilsView.getLeftButton(() => this.onBack())}
				/>

				<SegmentedView style={{flex: 1, }} type='carousel' barStyle={{height: 45, borderBottomColor: '#f2f2f2', borderBottomWidth: 1, }} indicatorPositionPadding={0}>
				  	<View title='快讯' titleStyle={{color: '#666', fontSize: 15, }} activeTitleStyle={{color: GlobalStyles.themeColor, fontSize: 15, fontWeight: 'bold', }} style={{backgroundColor: '#fff', flex: 1}}>
				      	<MyHighlightsTabItemView {...this.props}/>
				  	</View>
				  	<View title='动态' titleStyle={{color: '#666', fontSize: 15, }} activeTitleStyle={{color: GlobalStyles.themeColor, fontSize: 15, fontWeight: 'bold', }} style={{backgroundColor: '#fff', flex: 1}}>
					    <MyNewsTabItemView {...this.props}/>
				  	</View>
				  	<View title='视频' titleStyle={{color: '#666', fontSize: 15, }} activeTitleStyle={{color: GlobalStyles.themeColor, fontSize: 15, fontWeight: 'bold', }} style={{backgroundColor: '#fff', flex: 1}}>
					    <MyVideoTabItemView {...this.props}/>
				  	</View>
				  	<View title='影像' titleStyle={{color: '#666', fontSize: 15, }} activeTitleStyle={{color: GlobalStyles.themeColor, fontSize: 15, fontWeight: 'bold', }} style={{backgroundColor: '#fff', flex: 1}}>
					    <MyMovieTabItemView {...this.props}/>
				  	</View>
				  	<View title='图说' titleStyle={{color: '#666', fontSize: 15, }} activeTitleStyle={{color: GlobalStyles.themeColor, fontSize: 15, fontWeight: 'bold', }} style={{backgroundColor: '#fff', flex: 1}}>
					    <MyPicTabItemView {...this.props}/>
				  	</View>
				  	<View title='读报' titleStyle={{color: '#666', fontSize: 15, }} activeTitleStyle={{color: GlobalStyles.themeColor, fontSize: 15, fontWeight: 'bold', }} style={{backgroundColor: '#fff', flex: 1}}>
					    <MyNewspaperTabItemView {...this.props}/>
				  	</View>
				  	<View title='通告' titleStyle={{color: '#666', fontSize: 15, }} activeTitleStyle={{color: GlobalStyles.themeColor, fontSize: 15, fontWeight: 'bold', }} style={{backgroundColor: '#fff', flex: 1}}>
					    <MyNoticeTabItemView {...this.props}/>
				  	</View>
				</SegmentedView>

			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: GlobalStyles.bgColor,
	},
});
