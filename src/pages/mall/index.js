/**
 * 速芽物流 - 公共详情页
 * http://www.sdust.edu.cn/
 * @Meng
 */

import React, { Component } from 'react';
import {
	View,
	Text,
	Image,
	WebView,
	Dimensions,
	StyleSheet,
	StatusBar,
	TouchableOpacity,
	Platform
} from 'react-native';

import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'
import *as wechat from 'react-native-wechat'
import ShareUtils from '../../components/common/shareUtils'
import Theme from '../../components/common/Theme'
const WEBVIEW_REF = 'webview';

export default class Mall extends Component {

	constructor(props){
		super(props);
		const { params } = this.props.navigation.state;
		this.state={
			url: 'http://m.lswuyu.com/mobile',
			title: '',
		}
		this.netRequest = new NetRequest();
	}

	componentDidMount(){
		
	}

	updateState= (state) => {
		if (!this) { return };
		this.setState(state);
	}

	loadNetData = () => {
		
	}

	goBack = () => {
		this.webview.goBack();
	};



	render() {
		return (
			<View style={styles.container}>
				<StatusBar
					animated = {true}
					hidden = {true}
					backgroundColor = {'#0068B7'}
					translucent = {false}
					barStyle = {'default'}
				/>
				<View style={styles.mall}></View>
				<WebView
					ref={(webView) => {this.webview = webView}}
					startInLoadingState={true}
					source={{uri: this.state.url}}
					style={styles.webContainer}
					renderLoading={() => {
                        return <View style={GlobalStyles.loadingWrap}><Image source={require('../../assets/images/icons/loading.gif')} style={GlobalStyles.loadingIco} /></View>
                    }}
				/>	
			</View>
		);
	}

}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#ffffff',
		marginTop: -20,
	},
	mall: {
		width: GlobalStyles.width,
		height: Theme.statusBarHeight
	},
	webContainer: {
		flex: 1,
		backgroundColor: '#f2f2f2',
		width: GlobalStyles.width,
		// marginTop: Platform.OS == 'ios' ? -Theme.statusBarHeight + 20: Theme.statusBarHeight - 20,
	},
});