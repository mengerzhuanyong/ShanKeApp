/**
 * 掌上山科APP
 * http://www.sdust.edu.cn/
 * 山科技术团队倾情打造
 */

import React, {Component} from 'react'
import {
	Text,
	View,
	Image,
	TextInput,
	ScrollView,
	StyleSheet,
	TouchableOpacity
} from 'react-native'

import NetRequest from '../../utils/utilsRequest'
import { NetApi } from '../../constants/GlobalApi'
import GlobalStyles from '../../constants/GlobalStyles'
import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'

export default class Index extends Component {

	constructor(props) {
		super(props);
		this.state =  {}
		this.netRequest = new NetRequest();
	}

	componentDidMount(){
		this.loadNetData();
	}

	onBack = () => {
		this.props.navigation.goBack();
	}

	toUser = () => {
		const { navigate } = this.props.navigation;
		navigate('User', {
			data: '123'
		})
	}

	updateState= (state) => {
		if (!this) { return };
		this.setState(state);
	}

	loadNetData = () => {
		
	}

	render(){
		return (
			<View style={styles.container}>
				<NavigationBar
					title = {'教师入口'}
					leftButton = {UtilsView.getLeftUserButton(() => this.toUser())}
				/>
				<View style={styles.qidai}>
					<Image source={require('../../assets/images/icons/icon_qidai.png')} style={styles.qidaiimg} />
					<Text style={styles.qidaip}>即将上线，敬请期待！</Text>
				</View>
				<View style={[styles.jiugongge, GlobalStyles.flexRowCenter]}>
					<TouchableOpacity onPress={()=>{toastShort('即将上线，敬请期待！')}} style={[styles.jiugongge_item, GlobalStyles.flexColumnCenter, {backgroundColor: '#f60'}]}>
						<Image source={require('../../assets/images/icons/icon_baozhang.png')} style={styles.jiugongge_icon} />
						<Text style={styles.jiugongge_text}>财务报表</Text>
					</TouchableOpacity>
					<TouchableOpacity onPress={()=>{toastShort('即将上线，敬请期待！')}} style={[styles.jiugongge_item, GlobalStyles.flexColumnCenter, {backgroundColor: '#3292ff'}]}>
						<Image source={require('../../assets/images/icons/icon_jiaowu.png')} style={styles.jiugongge_icon} />
						<Text style={styles.jiugongge_text}>教务系统</Text>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: GlobalStyles.bgColor,
	},
	qidai:{
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		display: 'none'
	},
	qidaiimg:{
		width: 100,
		height: 100,
		
	},
	qidaip:{
		lineHeight: 70,
		fontSize:22,
		color:'#979797',
	},
	jiugongge: {
		width: GlobalStyles.width,
		height: 150,

	},
	jiugongge_item: {
		flex: 1,
		height: 150,
	},
	jiugongge_icon: {
		height: 60,
		width: 60,
	},
	jiugongge_text: {
		marginTop: 10,
		fontSize: 16,
		color: '#fff'
	},
});
