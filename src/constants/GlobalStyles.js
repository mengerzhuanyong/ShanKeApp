/**
 * 速芽物流 - 全局样式
 * https://menger.me
 * @大梦
 */

import {
	Platform,
	Dimensions,
} from 'react-native'

const isIOS = Platform.OS === 'ios';
const {	width, height } = Dimensions.get('window');
const themeColor = '#0068B7';
module.exports = {
	width: width,
	height: height,
	statusBar_Height_Ios: 44,
	statusBar_Height_Android: 50,
	
	bgColor: '#eeeff3',
	themeColor: themeColor,
	textColor: themeColor,

	bannerContainer: {
		height: 200,
	},
	bannerViewWrap: {
		flex: 1,
		position: 'relative',
	},
	bannerImg: {
		width: width,
		// height: 200,
        height: width*420/750,
		resizeMode: 'cover'
	},
	bannerTxt: {
		position: 'absolute',
		bottom: 10,
		width: width,
		height: 30,
		lineHeight: 30,
		fontSize:16,
		fontWeight:'bold',
		color:'#fff',
		textAlign:'center'
	},
	bannerShadow:{
		position: 'absolute',
		width:width,
		height:200,
		top:0,
		left:0,
		backgroundColor:'rgba(0,0,0,0.14)',
	},
	bannerDot: {
		width: 8,
		height: 8,
		marginTop: 2,
		borderRadius: 8,
		marginHorizontal: 5,
		backgroundColor: '#fff',
	},
	bannerActiveDot: {
		width: 18,
		height: 8,
		marginTop: 2,
		borderRadius: 8,
		marginHorizontal: 5,
		backgroundColor: '#fff',
	},
	newsinfo:{
		display:'flex',
		flexDirection:'row',
		justifyContent:'space-between',
		alignItems:'center',
		marginTop:10
	},
	newswriter:{
		fontSize: 12,
		color: '#989898',
		lineHeight: 20
	},
	newsdata:{
		fontSize: 12,
		color: '#989898',
		lineHeight: 20
	},
	newsnum:{
		display:'flex',
		flexDirection:'row',
		alignItems:'center',
		justifyContent:'center'
	},
	viewimg:{
		width:12,
		height:12,
		marginRight:2
	},
	viewnum:{
		fontSize: 12,
		color: '#989898',
		lineHeight: 20
	},
	newsnumwrap:{
		display:'flex',
		flexDirection:'row',
		justifyContent:'flex-end',
		alignItems:'center'
	},
	newsmore:{
		width:20,
		height:3,
		marginLeft:15
	},
	loadingWrap: {
		position: 'absolute',
		top: height*0.4,
		left: width*0.5,
		width: 70,
		height: 70,
		marginLeft: -35,
		marginTop: -35
	},
	loadingIco: {
		width: 70,
		height: 70
	},
	usermid:{

	},
	userlist:{
		height:50,
		display: 'flex',
		flexDirection: 'row',
		alignItems:'center',
		justifyContent:'space-between',
		borderBottomColor:'#f2f2f2',
		borderBottomWidth: 1,
		paddingLeft:15,
		paddingRight:15,
	},
	userlistleft:{
		width:30
	},
	usericon:{
		width:18,
		height:18
	},
	userlistright:{
		flex: 1,
		display:'flex',
		flexDirection:'row',
		alignItems:'center',
		justifyContent:'space-between'
	},
	userlisttext:{
		color: '#666',
		fontSize: 15
	},
	userlistmore:{
		width: 12,
		height: 12
	},
    mcell:{
        marginTop:10,
        backgroundColor:'#fff',
        position:'relative',
        zIndex:1
    },
    cellItem:{
        position:'relative',
        display:'flex',
        overflow:'hidden',
        marginLeft:12,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        borderBottomWidth:1,
        borderBottomColor:'#ececec',
    },
    cellLeft:{
        color:'#333',
        fontSize:15,
        alignItems:'center',
        width:80,
    },
    cellRight:{
        height:50,
        width:  width - 80,
        position: 'relative',
    },
    cellInput:{
        width:  width - 150,
        height:50,
        paddingRight: 10,
        fontSize:15,
        textAlign:'right',
        color:'#666',
    },
    inputAndroid:{
        padding: 0,
    },
    mcelltitle:{
        paddingLeft:10,
        paddingRight:10,
        marginTop:10,
    },
    btn:{
        backgroundColor:'#448cee',
        width: width*0.9,
        marginLeft:  width*0.05,
        overflow:'hidden',
        height:48,
        borderRadius:5,
        marginTop:10,
        alignItems:'center',
        justifyContent:'center'
    },
    btna:{
        color:'#fff',
        textAlign:'center',
        fontSize:18,
    },
    mt10:{
        marginTop:10
    },
    textRight:{
        justifyContent:'center',
        alignItems:'flex-end',
        width: width-130
    },
    cellRightIco:{
        width:12,
        height:16,
        marginRight:10
    },
    cellRightText:{
        color:'#666',
        marginRight:10,
    },
    selectView: {
        position: 'absolute',
        top: 0,
        right: 10,
        width: 100,
        height: 50,
    },
    selectViewWrap: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: 100,
    },
    paymentMethodTitleView: {
        width: 100,
        height: 50,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    cargoAttributesTitle: {
        textAlign: 'right',
        fontSize: 14,
        color: '#666',
    },
    dropdownStyle: {
        width: 130,
        marginRight: -10,
    },
    dropdownRow: {
        height: 40,
        justifyContent: 'center',
    },
    dropdownRowText: {
        fontSize: 14,
        color: '#666',
        textAlign: 'right',
        marginRight: 10
    },
    hide: {
    	display: 'none',
    },
    flexRowStart: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    flexRowEnd: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    flexRowStartStart: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    flexRowCenter: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    flexRowAround: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    flexRowBetween: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    flexColumnCenter: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    flexColumnBetween: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'flex-start'
    },
    whiteModule: {
        marginTop: 10,
        backgroundColor: '#fff',

    },
	

	
};