/**
 * 速芽物流 - API
 * https://menger.me
 * UpdateTime: 2017/12/25 14:55
 * @大梦
 */


export const NetApi = {

	base: 'http://app.sdust.edu.cn/index/',
	root: 'http://app.sdust.edu.cn/',
	json: '/type/json/',

	login: 'login/login',	// 登录
	register: 'login/register',	// 注册
	forget: 'login/forget',	// 忘记密码
	sendSMS: 'login/sendSMS',	//  发送短信   1注册  2找回密码  3修改手机号
	logout: 'login/logout',	// 退出
	user: 'personal/user_info',	// 个人中心
	edituser: 'personal/edit_user',	// 修改用户信息
	relation: 'personal/relation',	// 用户关系
	mycollect: 'personal/collect',	// 我的收藏
	coop: 'personal/coop',	// 关于我们
	xieyi: 'Personal/xieyi',	// 关于我们

	setPassword: 'personal/upPassword',   //修改密码   password  uid
	setMobile: 'personal/upMobile',   //修改手机号    mobile   uid

	newslist: 'index/index',	// 新闻    1要闻 2报纸 3视频 4音频 5图文 6快讯
	newslide: 'index/slide',	// 新闻
	newsdetail: 'index/detail',	// 新闻详情     isapp   0浏览器内页面   1app内调数据   2app内嵌页面
	imagedetail: 'index/image_detail',  //图说详情
	paper: 'paper/index',   //报纸列表   get id期数
	plate: 'paper/plate',   //报纸栏目
	paperdetail: 'paper/detail',  //报纸详情  
	paperCollect: 'paper/is_collect',  //获取报纸详情的收藏状态

	collect: 'index/collect', //收藏新闻


	is_online: 'index/is_online', //是否上架
	system_config: 'index/system_config', //是否上架

	
	
}