/**
 * 速芽物流 - API
 * https://menger.me
 * UpdateTime: 2017/12/25 14:55
 * @大梦
 */

'use strict';

const VERSION_DATA = require('../../app.json');

export const Constants = {
	// VERSION_CODE
	VERSION_CODE: VERSION_DATA.version_code,
	// VERSION_NAME
	VERSION_NAME: VERSION_DATA.version_name,	
}